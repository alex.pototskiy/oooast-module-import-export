<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 11:18
 */

namespace OooAst\ImportExport\Model\CustomerGroup\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\InputException;
use Magento\Tax\Api\TaxClassRepositoryInterface;

/**
 * Class SourceTaxClass
 *
 * @package OooAst\ImportExport\Model\Export\CustomerGroup\Source
 */
class SourceTaxClass extends AbstractSource
{
    /**
     * Tax classes options
     *
     * @var array
     */
    private $options;

    /**
     * SourceTaxClass constructor.
     *
     * @param TaxClassRepositoryInterface $taxClassRepository
     * @param SearchCriteriaBuilder $criteriaBuilder
     *
     * @throws InputException
     */
    public function __construct(
        TaxClassRepositoryInterface $taxClassRepository,
        SearchCriteriaBuilder $criteriaBuilder
    ) {
        $criteria = $criteriaBuilder
            ->addFilter('class_type', 'CUSTOMER', 'eq')
            ->create();
        foreach ($taxClassRepository->getList($criteria)->getItems() as $taxClass) {
            $this->options[] = [
                'value' => $taxClass->getClassId(),
                'label' => $taxClass->getClassName()
            ];
        }
    }

    /**
     * Get all tax classes as options
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        return $this->options;
    }
}
