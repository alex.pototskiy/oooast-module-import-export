<?php
/**
 * PHP version 7.1
 * Customer Group import/export support data
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 8:12
 */

namespace OooAst\ImportExport\Model\CustomerGroup;


trait CustomerGroup
{
    protected $columnToHeaderMap = [
        'tax_class_id' => 'tax_class'
    ];
    protected $headerToColumnMap = [];

    public function __construct()
    {
        $this->headerToColumnMap = array_flip($this->columnToHeaderMap);
    }
}
