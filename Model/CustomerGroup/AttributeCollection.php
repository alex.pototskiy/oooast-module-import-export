<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 10:41
 */

namespace OooAst\ImportExport\Model\CustomerGroup;

use Exception;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Framework\Data\Collection;
use Magento\ImportExport\Model\Export\Factory as CollectionFactory;
use OooAst\ImportExport\Model\CustomerGroup\Source\SourceTaxClass;

/**
 * Class AttributeCollectionProvider
 *
 * @package OooAst\Core\Model\Customer\Export
 */
class AttributeCollection
{
    /**
     * Attribute collection
     * @var Collection
     */
    private $collection;
    /**
     * Attribute factory
     * @var AttributeFactory
     */
    private $attributeFactory;

    /**
     * AttributeCollectionProvider constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param AttributeFactory $attributeFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        AttributeFactory $attributeFactory
    ) {
        $this->collection = $collectionFactory->create(Collection::class);
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * Get attribute collection
     *
     * @return Collection
     * @throws Exception
     */
    public function get(): Collection
    {
        if (count($this->collection) == 0) {
            /**
             * @var Attribute $groupIdAttribute
             */
            $groupIdAttribute = $this->attributeFactory->create();
            $groupIdAttribute->setId('customer_group_id');
            $groupIdAttribute->setDefaultFrontendLabel('Customer Group ID');
            $groupIdAttribute->setAttributeCode('customer_group_id');
            $groupIdAttribute->setBackendType('int');
            $this->collection->addItem($groupIdAttribute);
            /**
             * @var Attribute $groupCodeAttribute
             */
            $groupCodeAttribute = $this->attributeFactory->create();
            $groupCodeAttribute->setId("customer_group_code");
            $groupCodeAttribute->setDefaultFrontendLabel("Customer Group");
            $groupCodeAttribute->setAttributeCode("customer_group_code");
            $groupCodeAttribute->setBackendType("varchar");
            $this->collection->addItem($groupCodeAttribute);
            /**
             * @var Attribute $groupTaxClass
             */
            $groupTaxClass = $this->attributeFactory->create();
            $groupTaxClass->setId("tax_class_id");
            $groupTaxClass->setDefaultFrontendLabel("Tax Class");
            $groupTaxClass->setAttributeCode("tax_class_id");
            $groupTaxClass->setBackendType("int");
            $groupTaxClass->setFrontendInput("multiselect");
            $groupTaxClass->setSourceModel(SourceTaxClass::class);
            $this->collection->addItem($groupTaxClass);
        }
        return $this->collection;
    }
}
