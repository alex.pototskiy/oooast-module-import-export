<?php
/**
 * PHP version 7.1
 * Customer Group import/export constants
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 8:32
 */

namespace OooAst\ImportExport\Model\CustomerGroup;


interface Constants
{
    const ENTITY_TYPE_CODE = 'customer_group';
    const COLUMN_ID = 'customer_group_id';
    const COLUMN_CODE = 'customer_group_code';
    const COLUMN_TAX_ID = 'tax_class_id';
    const HEADER_TAX_CLASS = 'tax_class';
}
