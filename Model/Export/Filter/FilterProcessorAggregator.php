<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 13:27
 */

namespace OooAst\ImportExport\Model\Export\Filter;

use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Exception\LocalizedException;

class FilterProcessorAggregator
{
    /**
     * Filter processors
     *
     * @var FilterProcessorInterface[]
     */
    private $handler;

    /**
     * FileProcessorAggregator constructor.
     *
     * @param FilterProcessorInterface[] $handler The filter processors
     *
     * @throws LocalizedException
     */
    public function __construct(array $handler = [])
    {
        $this->handler = $handler;
        $this->validateProcessorType();
    }

    /**
     * Validate the filter processor types
     *
     * @throws LocalizedException
     */
    private function validateProcessorType(): void
    {
        foreach ($this->handler as $typeProcessors) {
            foreach ($typeProcessors as $filterProcessor) {
                if (!($filterProcessor instanceof FilterProcessorInterface)) {
                    throw new LocalizedException(
                        __(
                            'Filter handler must be instance of "%interface%"',
                            ['interface' => FilterProcessorInterface::class]
                        )
                    );
                }
            }
        }
    }

    /**
     * @param string $backendType The column type
     * @param string $frontendInput The column frontend input
     * @param Collection $collection
     * @param string $column
     * @param mixed $value
     *
     * @throws LocalizedException
     */
    public function process(string $backendType, string $frontendInput, Collection $collection, string $column, $value)
    {
        if (!isset($this->handler[$backendType]) || !isset($this->handler[$backendType][$frontendInput])) {
            throw  new LocalizedException(
                __(
                    'No filter processor for "%type" with frontend "%input".',
                    ['type' => $backendType, 'input' => $frontendInput]
                )
            );
        }
        /** @var FilterProcessorInterface $filterProcessor */
        $filterProcessor = $this->handler[$backendType][$frontendInput];
        $filterProcessor->process($collection, $column, $value);
    }
}
