<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 13:19
 */

namespace OooAst\ImportExport\Model\Export\Filter;

use Magento\Customer\Model\ResourceModel\Group\Collection;

class RangeIntFilter implements FilterProcessorInterface
{

    /**
     * @param Collection $collection The item collection
     * @param string $column The filter column name
     * @param mixed $value The filter value
     */
    public function process(Collection $collection, string $column, $value): void
    {
        if (is_array($value)) {
            $from = $value[0] ?? null;
            $to = $value[1] ?? null;
            if (is_numeric($from) && !empty($from)) {
                $collection->addFieldToFilter($column, ['from' => $from]);
            }
            if (is_numeric($to) && !empty($to)) {
                $collection->addFieldToFilter($column, ['to' => $to]);
            }
            return;
        }
        $collection->addFieldToFilter($column, ['eq' => $value]);
    }
}
