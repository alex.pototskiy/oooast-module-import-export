<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 13:19
 */

namespace OooAst\ImportExport\Model\Export\Filter;

use Magento\Customer\Model\ResourceModel\Group\Collection;

class InIntFilter implements FilterProcessorInterface
{

    /**
     * @param Collection $collection The item collection
     * @param string $column The filter column name
     * @param mixed $value The filter value
     */
    public function process(Collection $collection, string $column, $value): void
    {
        if ($value == null) {
            return;
        }
        $data = $value;
        if (!is_array($value)) {
            $data = [$value];
        }
        if (!empty($data)) {
            $data = array_map(
                function ($v) {
                    return (int)$v;
                },
                $data
            );
            $collection->addFieldToFilter($column, ['in' => $data]);
        }
    }
}
