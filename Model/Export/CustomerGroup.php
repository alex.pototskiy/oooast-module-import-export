<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 11.12.2018
 * Time: 17:58
 */

namespace OooAst\ImportExport\Model\Export;

use Exception;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\ImportExport\Model\Export\AbstractEntity;
use Magento\ImportExport\Model\Export\Factory;
use Magento\ImportExport\Model\ResourceModel\CollectionByPagesIteratorFactory;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\ImportExport\Model\ColumnProvider;
use OooAst\ImportExport\Model\CustomerGroup\AttributeCollection;
use OooAst\ImportExport\Model\CustomerGroup\Constants;
use OooAst\ImportExport\Model\Traits\GetOrDefault;

/**
 * Class CustomerGroup
 *
 * @package OooAst\ImportExport\Model\Export
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomerGroup extends AbstractEntity implements Constants
{
    use GetOrDefault;
    use \OooAst\ImportExport\Model\CustomerGroup\CustomerGroup;

    /**
     * Group collection
     *
     * @var Collection
     */
    private $collection;
    /**
     * Attribute collection provider
     *
     * @var CustomerGroup\AttributeCollection
     */
    private $attributeCollection;
    /**
     * The column provider
     *
     * @var ColumnProvider
     */
    private $columnProvider;
    /**
     * Group collection factory
     *
     * @var ColumnProvider
     */
    private $collectionBuilder;
    /**
     * Attribute values
     *
     * @var array
     */
    private $attrValues;

    /**
     * CustomerGroup constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Factory $collectionFactory
     * @param CollectionByPagesIteratorFactory $resourceColFactory
     * @param Collection $collection
     * @param CustomerGroup\AttributeCollection $attributeCollection
     * @param ColumnProvider $columnProvider
     * @param CustomerGroup\CollectionBuilderFactory $collectionBuilder
     * @param array $data
     *
     * @throws Exception
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Factory $collectionFactory,
        CollectionByPagesIteratorFactory $resourceColFactory,
        Collection $collection,
        AttributeCollection $attributeCollection,
        ColumnProvider $columnProvider,
        CustomerGroup\CollectionBuilderFactory $collectionBuilder,
        array $data = []
    ) {
        $this->collection = $collection;
        $this->attributeCollection = $attributeCollection;
        $this->columnProvider = $columnProvider;
        $this->collectionBuilder = $collectionBuilder->create(
            [
                'columnProvider' => $this->columnProvider
            ]
        );
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $collectionFactory,
            $resourceColFactory,
            $data
        );
        $this
            ->_initStores()
            ->_initWebsites()
            ->initAttributeValues();
    }

    /**
     * Init values of attribute with options
     *
     * @throws Exception
     */
    private function initAttributeValues()
    {
        /** @var Attribute $attr */
        $attr = $this->attributeCollection->get()->getItemById(self::COLUMN_TAX_ID);
        foreach ($attr->getSource()->toOptionArray() as $value) {
            $this->attrValues[self::COLUMN_TAX_ID][$value['value']] = $value['label'];
        };
    }

    /**
     * Export process
     *
     * @return string
     * @throws LocalizedException
     * @throws Exception
     */
    public function export()
    {
        set_time_limit(0); // phpcs:ignore
        $page = 1;
        $writer = $this->getWriter();
        do {
            if ($page == 1) {
                $writer->setHeaderCols($this->_getHeaderColumns());
            }

            $collection = $this->collectionBuilder->create(
                $this->getAttributeCollection(),
                $this->getOrDefault($this->_parameters['skip_attr'], []),
                $this->_parameters['export_filter']
            );
            $collection->setPageSize(100);
            $collection->setCurPage($page);

            foreach ($collection->getData() as $data) {
                $this->replaceTaxClassWithValue($data);
                $exportData = [];
                foreach ($data as $key => $value) {
                    if (isset($this->columnToHeaderMap[$key])) {
                        $exportData[$this->columnToHeaderMap[$key]] = $value;
                    } else {
                        $exportData[$key] = $value;
                    }
                }
                $writer->writeRow($exportData);
            }
            $page++;
        } while ($collection->getCurPage() < $collection->getLastPageNumber());

        return $writer->getContents();
    }

    /**
     * @inheritdoc
     * @throws LocalizedException
     * @throws Exception
     */
    protected function _getHeaderColumns()
    {
        return $this->columnProvider->getHeaders(
            $this->getAttributeCollection(),
            $this->getOrDefault($this->_parameters['skip_attr'], [])
        );
    }

    /**
     * Get attribute collection
     *
     * @return \Magento\Framework\Data\Collection
     * @throws Exception
     */
    public function getAttributeCollection()
    {
        return $this->attributeCollection->get();
    }

    /**
     * @param $data array
     *
     * @throws Exception
     */
    private function replaceTaxClassWithValue(array &$data)
    {
        if (isset($this->attrValues[self::COLUMN_TAX_ID][$data[self::COLUMN_TAX_ID]])) {
            $data[self::COLUMN_TAX_ID] = $this->attrValues[self::COLUMN_TAX_ID][$data[self::COLUMN_TAX_ID]];
        } else {
            throw new LocalizedException(
                __('Cannot find label for tax class id "%id".', ['id' => $data[self::COLUMN_TAX_ID]])
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function exportItem($item)
    {
        // nothing to do
    }

    /**
     * Entity type code getter
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE_CODE;
    }

    public function filterAttributeCollection(\Magento\Framework\Data\Collection $collection)
    {
        return $collection;
    }

    /**
     * @inheritdoc
     */
    protected function _getEntityCollection()
    {
    }

}
