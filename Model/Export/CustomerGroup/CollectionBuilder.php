<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 12:51
 */

namespace OooAst\ImportExport\Model\Export\CustomerGroup;

use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Data\Collection as AttributeCollection;
use Magento\Framework\Exception\LocalizedException;
use OooAst\ImportExport\Api\CollectionBuilderInterface;
use OooAst\ImportExport\Api\ColumnProviderInterface;
use OooAst\ImportExport\Model\Export\Filter\FilterProcessorAggregator;

/**
 * Class CollectionBuilder
 *
 * @package OooAst\ImportExport\Model\Export\CustomerGroup
 */
class CollectionBuilder implements CollectionBuilderInterface
{
    /**
     * Column provider
     *
     * @var ColumnProviderInterface
     */
    private $columnProvider;
    /**
     * Group collection factory
     *
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * Filter processor
     *
     * @var FilterProcessorAggregator
     */
    private $filterProcessor;

    /**
     * GroupCollectionFactory constructor.
     *
     * @param ColumnProviderInterface $columnProvider The column provider
     * @param CollectionFactory $collectionFactory The collection factory
     * @param FilterProcessorAggregator $filterProcessor The filter processor
     */
    public function __construct(
        ColumnProviderInterface $columnProvider,
        CollectionFactory $collectionFactory,
        FilterProcessorAggregator $filterProcessor
    ) {
        $this->columnProvider = $columnProvider;
        $this->collectionFactory = $collectionFactory;
        $this->filterProcessor = $filterProcessor;
    }

    /**
     * SourceItemCollection is used to gather all the data (with filters applied) which need to be exported
     *
     * @param AttributeCollection $attributes The attribute collection
     * @param array $skipAttrs Attributes to skip
     * @param array $filters The filter array
     *
     * @return Collection
     * @throws LocalizedException
     */
    public function create(AttributeCollection $attributes, array $skipAttrs, array $filters): Collection
    {
        $collection = $this->collectionFactory->create();
        $columns = $this->columnProvider->getColumns($attributes, $skipAttrs);
        $collection->addFieldToSelect($columns);
        foreach ($this->retrieveFilterData($filters) as $columnName => $value) {
            /** @var Attribute $attrDef */
            $attrDef = $attributes->getItemById($columnName);
            if (!$attrDef) {
                throw new LocalizedException(
                    __(
                        'Given column name "%columnName" is not present in collection.',
                        ['columnName' => $columnName]
                    )
                );
            }

            $backendType = $attrDef->getBackendType();
            $frontendInput = $attrDef->getFrontendInput() ?? 'input';
            if (!$backendType || !$frontendInput) {
                throw new LocalizedException(
                    __(
                        'There is no backend type or frontend input for column "%columnName".',
                        ['columnName' => $columnName]
                    )
                );
            }
            $this->filterProcessor->process($backendType, $frontendInput, $collection, $columnName, $value);
        }
        return $collection;
    }

    private function retrieveFilterData(array $filters): array
    {
        /** @noinspection PhpArrayFilterCanBeConvertedToLoopInspection */
        return array_filter(
            $filters ?? [],
            function ($value) {
                return $value !== '';
            }
        );
    }
}
