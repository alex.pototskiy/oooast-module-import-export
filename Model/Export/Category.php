<?php
/**
 * PHP version 7.1
 *
 * Category export model
 *
 * @category ImportExport
 * @package  OooAst_CatetoryImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 08.04.2019
 * Time: 20:22
 */

namespace OooAst\ImportExport\Model\Export;

use DateTime;
use Exception;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\ImportExport\Model\Export;
use Magento\ImportExport\Model\Export\Entity\AbstractEav;
use Magento\ImportExport\Model\Export\Factory;
use Magento\ImportExport\Model\ResourceModel\CollectionByPagesIteratorFactory as ColIteratorFactoryAlias;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\Catalog\Api\Data\CategoryGroupCodeInterface;
use OooAst\ImportExport\Model\Export\Category\CategoryExportAttributeList;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Traits\Category\ConstantsInterface;
use Psr\Log\LoggerInterface;

/**
 * Category export model class
 *
 * @category ImportExport
 * @package  OooAst_CatetoryImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Category extends AbstractEav implements ConstantsInterface
{
    /**
     * Permanent entity columns.
     *
     * @var string[]
     *
     * @SuppressWarnings(PHPMD.CamelCasePropertyName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore
    protected $_permanentAttributes = [
        self::COL_STORE,
        self::COL_WEBSITE,
        self::COL_PARENT_SKG,
        self::COL_SKG
    ];

    /**
     * Special attributes
     *
     * @var array
     */
    protected $specialAttributes = [
        self::COL_STORE,
        self::COL_WEBSITE,
        self::COL_PARENT_SKG
    ];
    /**
     * Attributes codes which shows as date
     *
     * @var array
     */
    protected $datetimeAttrCodes = [];

    /**
     * Category entities collection
     *
     * @var Collection
     */
    private $categoryCollection;
    /**
     * Category entity type code
     *
     * @var string|null
     */
    private $entityTypeCode;
    /**
     * Category repository
     *
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * Category collection factory
     *
     * @var CollectionFactory
     */
    private $categoryColFactory;
    /** @noinspection PhpUndefinedClassInspection */
    /**
     * Magento logger
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Category constructor.
     *
     * @param ScopeConfigInterface $scopeConfig The configuration scope
     * @param StoreManagerInterface $storeManager The store manager
     * @param Factory $collectionFactory The category collection factory
     * @param ColIteratorFactoryAlias $resourceColFactory The category collection iterator
     * @param TimezoneInterface $localeDate The locale date interface
     * @param Config $eavConfig The EAV configuration
     * @param CollectionFactory $categoryColFactory The category collection factory
     * @param CategoryRepository $categoryRepository The category repository
     * @param LoggerInterface $logger The Magento logger
     * @param CategoryExportAttributeList $attributeList
     * @param array $data The export parameters
     *
     * @throws LocalizedException
     * @throws Exception
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Factory $collectionFactory,
        ColIteratorFactoryAlias $resourceColFactory,
        TimezoneInterface $localeDate,
        Config $eavConfig,
        CollectionFactory $categoryColFactory,
        CategoryRepository $categoryRepository,
        LoggerInterface $logger,
        CategoryExportAttributeList $attributeList,
        array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->logger = $logger;
        $mainCollection = $categoryColFactory->create();
        $data['entity_type_id'] = $mainCollection->getEntity()->getEntityType()->getId();
        $this->entityTypeCode = $mainCollection->getEntity()->getEntityType()->getEntityTypeCode();
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $collectionFactory,
            $resourceColFactory,
            $localeDate,
            $eavConfig,
            $data
        );
        $this->_attributeCollection = $attributeList->getExportAttributes($this->_permanentAttributes);
        $this->categoryColFactory = $categoryColFactory;
        $this->_initAttributeValues()
            ->_initAttributeTypes()
            ->_initStores()
            ->_initWebsites();
    }

    /**
     * EAV entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return $this->entityTypeCode;
    }

    /**
     * Get entities to export
     *
     * @param bool $resetCollection Create clear collection
     *
     * @return Collection
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore
    /**
     * Export process.
     *
     * @return string
     * @throws Exception
     */
    public function export()
    {
        set_time_limit(0);
        $writer = $this->getWriter();
        $page = 0;
        while (true) {
            ++$page;
            $entityCollection = $this->_getEntityCollection(true);
            $entityCollection->setOrder('entity_id', 'asc');
            $entityCollection->setStoreId(Store::DEFAULT_STORE_ID);
            $this->_prepareEntityCollection($entityCollection);
            $entityCollection->setPage($page, 100);
            if ($entityCollection->count() == 0) {
                break;
            }
            $exportData = $this->getExportData();
            if ($page == 1) {
                $writer->setHeaderCols($this->_getHeaderColumns());
            }
            foreach ($exportData as $dataRow) {
                $writer->writeRow($dataRow);
            }
            if ($entityCollection->getCurPage() >= $entityCollection->getLastPageNumber()) {
                break;
            }
        }
        return $writer->getContents();
    }

    /**
     * @inheritDoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidFunctionName.PrivateNoUnderscore
    protected function _getEntityCollection($resetCollection = false): Collection
    {
        if ($resetCollection || empty($this->categoryCollection)) {
            $this->categoryCollection = $this->categoryColFactory->create();
        }
        $this->categoryCollection->addFieldToFilter('level', ['neq' => 0]);
        return $this->categoryCollection;
    }

    /**
     * Get categories data to export
     *
     * @return array
     */
    private function getExportData(): array
    {
        $exportData = [];
        try {
            $rawData = $this->collectRawData();
            foreach ($rawData as $categoryData) {
                foreach ($categoryData as $dataRow) {
                    $v = array_diff_key($dataRow, array_flip($this->specialAttributes));
                    unset($v[self::COL_SKG]);
                    if (!empty($v)) {
                        $exportData[] = $dataRow;
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->critical($e);
        }
        return $exportData;
    }

    /**
     * Collect category data
     *
     * @return array
     * @throws Exception
     */
    private function collectRawData()
    {
        $data = [];
        $items = $this->loadCollection();
        foreach ($items as $itemId => $itemByStore) {
            foreach (array_keys($this->_storeIdToCode) as $storeId) {
                /** @var \Magento\Catalog\Model\Category $item */
                $item = $itemByStore[$storeId];
                $this->collectDataFromItem($item, $storeId, $itemId, $data);
            }
        }
        return $data;
    }

    /**
     * Get entities
     *
     * @return array
     */
    private function loadCollection(): array
    {
        $data = [];
        $collection = $this->_getEntityCollection();
        foreach (array_keys($this->_storeIdToCode) as $storeId) {
            $collection->setOrder('entity_id', 'asc');
            $this->_prepareEntityCollection($collection);
            $collection->setStoreId($storeId);
            $collection->load();
            foreach ($collection as $itemId => $item) {
                $data[$itemId][$storeId] = $item;
            }
            $collection->clear();
        }
        return $data;
    }

    /**
     * Collect data for one row
     *
     * @param \Magento\Catalog\Model\Category $item
     * @param int $storeId
     * @param int $itemId
     * @param array $data
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function collectDataFromItem(
        \Magento\Catalog\Model\Category $item,
        $storeId,
        $itemId,
        array &$data
    ): void {
        $storeCode = $this->_storeIdToCode[$storeId];
        foreach ($this->_getExportAttributeCodes() as $code) {
            $attrValue = $item->getData($code);
            if (!$this->isValidAttribute($code, $attrValue)) {
                continue;
            }
            $attrValue = $this->prepareAttributeValue($code, $attrValue);
            if ($this->isValueAlreadyInDefaultStore($storeId, $itemId, $code, $attrValue, $data)) {
                continue;
            }
            $this->checkAndPrepareMultiselectOrScalar($item, $storeId, $itemId, $code, $attrValue, $data);
            $data[$itemId][$storeId][self::COL_STORE] = $storeCode;
            if ($storeId != Store::DEFAULT_STORE_ID) {
                $data[$itemId][$storeId][self::COL_WEBSITE] =
                    $this->_storeManager->getWebsite(
                        $this->_storeManager->getStore(
                            $storeId
                        )->getWebsiteId()
                    )->getCode();
            }
            $data[$itemId][$storeId][self::COL_SKG] = htmlspecialchars_decode(
                $item->getData(self::COL_SKG)
            );
            $data[$itemId][$storeId][self::COL_PARENT_SKG] = $item->getParentId() == 0
                ? CategoryGroupCodeInterface::ROOT_PARENT_SKG
                : $item->getParentCategory()->getExtensionAttributes()->getSkg();
        }
    }

    /**
     * Check that attribute is valid
     *
     * @param string $code The attribute code
     * @param mixed $value The attribute value
     *
     * @return bool
     */
    private function isValidAttribute($code, $value)
    {
        $isValid = true;
        if (!is_numeric($value) && empty($value)) {
            $isValid = false;
        }
        if (!isset($this->_attributeValues[$code])) {
            $isValid = false;
        }
        if (is_array($value)) {
            $isValid = false;
        }
        return $isValid;
    }

    /**
     * Get header names
     *
     * @return array The header names
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore

    /**
     * Prepare scalar, option and datetime attribute value
     *
     * @param string $code
     * @param mixed $attrValue
     *
     * @return string
     * @throws Exception
     */
    private function prepareAttributeValue($code, $attrValue): string
    {
        if (isset($this->_attributeValues[$code][$attrValue]) && !empty($this->_attributeValues[$code])) {
            $attrValue = $this->_attributeValues[$code][$attrValue];
        }
        if ($this->attributeTypeIsDatetime($code)) {
            /** @noinspection PhpComposerExtensionStubsInspection */
            $attrValue = $this->_localeDate->formatDateTime(new DateTime($attrValue));
        } elseif ($this->attributeTypes[$code] == 'datetime') {
            /** @noinspection PhpComposerExtensionStubsInspection */
            $attrValue = $this->_localeDate->formatDate(new DateTime($attrValue));
        }
        return $attrValue;
    }

    /**
     * Check if attribute type is date-time (not just date)
     *
     * @param string $code
     *
     * @return bool
     */
    private function attributeTypeIsDatetime($code): bool
    {
        return $this->attributeTypes[$code] == 'datetime' && in_array($code, $this->datetimeAttrCodes);
    }

    /**
     * Check if attribute value is exported as part of default store
     *
     * @param int $storeId
     * @param int $itemId
     * @param string $code
     * @param string $attrValue
     * @param array $data
     *
     * @return bool
     */
    private function isValueAlreadyInDefaultStore($storeId, $itemId, $code, string $attrValue, array &$data): bool
    {
        return $storeId != Store::DEFAULT_STORE_ID
            && isset($data[$itemId][Store::DEFAULT_STORE_ID][$code])
            && $data[$itemId][Store::DEFAULT_STORE_ID][$code] == htmlspecialchars_decode(
                $attrValue
            );
    }

    /**
     * Check if value is multiselect or scalar and prepare it
     *
     * @param \Magento\Catalog\Model\Category $item
     * @param int $storeId
     * @param int $itemId
     * @param string $code
     * @param mixed $attrValue
     * @param array $data
     *
     * @return array
     */
    private function checkAndPrepareMultiselectOrScalar(
        \Magento\Catalog\Model\Category $item,
        $storeId,
        $itemId,
        $code,
        $attrValue,
        array &$data
    ): array {
        if ($this->attributeTypes[$code] !== 'multiselect') {
            $data[$itemId][$storeId][$code] = htmlspecialchars_decode(
                $attrValue
            );
        } else {
            $data[$itemId][$storeId][$code] = $this->prepareMultiselectValue($item, $code);
        }
        return $data;
    }

    /**
     * @inheritDoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine

    /**
     * Prepare multiselect value for saving
     *
     * @param \Magento\Catalog\Model\Category $item The category
     * @param string $code The attribute code
     *
     * @return string
     */
    private function prepareMultiselectValue(
        \Magento\Catalog\Model\Category $item,
        string $code
    ) {
        $value = explode(',', $item->getData($code));
        $options = array_intersect_key(
            $this->_attributeValues[$code],
            array_flip($value)
        );
        return implode(',', $this->wrapValue($options));
    }

    /**
     * @inheritDoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidFunctionName.PrivateNoUnderscore
    /**
     * Wrap values with double quotes if "Fields Enclosure" option is enabled
     *
     * @param string|array $value
     *
     * @return string|array
     */
    private function wrapValue($value)
    {
        if (!empty($this->_parameters[Export::FIELDS_ENCLOSURE])) {
            $wrap = function ($value) {
                return sprintf('"%s"', str_replace('"', '""', $value));
            };

            $value = is_array($value) ? array_map($wrap, $value) : $wrap($value);
        }

        return $value;
    }

    /**
     * @inheritDoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore
    protected function _getHeaderColumns()
    {
        $columns = [];
        if (isset($this->_parameters[Export::FILTER_ELEMENT_SKIP]) &&
            !empty($this->_parameters[Export::FILTER_ELEMENT_SKIP])) {
            $skipAttrs = $this->_parameters[Export::FILTER_ELEMENT_SKIP];
            /**
             * @var Attribute $attr
             */
            foreach ($this->_attributeCollection->getItems() as $attr) {
                if (!array_key_exists($attr->getId(), $skipAttrs)) {
                    $columns[] = $attr->getAttributeCode();
                }
            }
        } else {
            $columns = array_map(
                function (Attribute $attr) {
                    return $attr->getAttributeCode();
                },
                $this->_attributeCollection->getItems()
            );
        }
        return array_merge($this->_permanentAttributes, $columns);
    }

    /**
     * Export one item
     *
     * @param \Magento\Catalog\Model\Category $item
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function exportItem($item)
    {
    }
}
