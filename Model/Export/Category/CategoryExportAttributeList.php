<?php
/**
 * PHP version 7.1
 * Category export attribute list
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 24.04.2019
 * Time: 15:45
 */

namespace OooAst\ImportExport\Model\Export\Category;

use Exception;
use Magento\Catalog\Api\CategoryAttributeRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Data\Collection;
use Magento\Framework\Data\CollectionFactory;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;

/**
 * Class CategoryExportAttributeList
 *
 * @package OooAst\ImportExport\Model\Export\Category
 */
class CategoryExportAttributeList
{
    /**
     * Category attribute repository
     *
     * @var CategoryAttributeRepositoryInterface
     */
    private $attributeRepository;
    /**
     * Search criteria helper
     *
     * @var SearchCriteriaHelper
     */
    private $criteriaHelper;
    /**
     * Collection factory
     *
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * CategoryExportAttributeList constructor.
     *
     * @param CategoryAttributeRepositoryInterface $attributeRepository
     * @param SearchCriteriaHelper $criteriaHelper
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CategoryAttributeRepositoryInterface $attributeRepository,
        SearchCriteriaHelper $criteriaHelper,
        CollectionFactory $collectionFactory
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->criteriaHelper = $criteriaHelper;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get collection of all visible plus permanent attributes
     *
     * @param array $permanentAttributes
     *
     * @return Collection
     * @throws Exception
     */
    public function getExportAttributes(array $permanentAttributes): Collection
    {
        $attributes = $this->collectionFactory->create();
        $criteria = $this->criteriaHelper->createEmptyCriteria();
        foreach ($this->attributeRepository->getList($criteria)->getItems() as $attribute) {
            if ($attribute->getIsVisible() || in_array($attribute->getAttributeCode(), $permanentAttributes)) {
                if ($attribute instanceof Attribute) {
                    $attributes->addItem($attribute);
                }
            }
        }
        return $attributes;
    }
}
