<?php
/**
 * PHP version 7.1
 * Import/Export help functions and classes
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 18.09.2019
 * Time: 6:50
 */

namespace OooAst\ImportExport\Model\Traits;


trait GetOrDefault
{
    function getOrDefault(&$var, $default = null) {
        return isset($var) ? $var : $default;
    }
}
