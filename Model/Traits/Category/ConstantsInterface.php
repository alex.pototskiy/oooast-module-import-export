<?php
/**
 * PHP version 7.1
 * Category import export constants
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 24.04.2019
 * Time: 11:35
 */

namespace OooAst\ImportExport\Model\Traits\Category;

/**
 * Category import/export constants
 *
 * @package OooAst\ImportExport\Model\Traits\Category
 */
interface ConstantsInterface
{
    const ACTION_DELETE = 'delete';
    const ACTION_ADD = 'add';
    const ACTION_UPDATE = 'update';

    const COL_SKG = 'skg';
    const COL_STORE = '_store';
    const COL_ID = 'entity_id';
    const COL_WEBSITE = '_website';
    const COL_PARENT_SKG = '_parent_group_code';

    /**
     * Pseudo multi line separator in one cell.
     *
     * Can be used as custom option value delimiter or in configurable fields cells.
     */
    const PSEUDO_MULTI_LINE_SEPARATOR = ',';

    /**
     * Symbol between Name and Value between Pairs.
     */
    const PAIR_NAME_VALUE_SEPARATOR = '=';

    /**
     * Data row scopes.
     */
    const SCOPE_DEFAULT = 1;
    const SCOPE_WEBSITE = 2;
    const SCOPE_STORE = 0;
    const SCOPE_NULL = -1;

    const SKG_MAX_LEN = 25;
}
