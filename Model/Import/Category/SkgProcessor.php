<?php
/**
 * PHP version 7.1
 *
 * Category import skg processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 17:29
 */

declare(strict_types=1);

namespace OooAst\ImportExport\Model\Import\Category;

use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use OooAst\Catalog\Api\Data\CategoryGroupCodeInterface;

/**
 * Skg processor to support category import
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class SkgProcessor
{
    const ATTR_SKG = 'skg';

    /**
     * Old (before import) category skgs
     *
     * [SKG] => array(
     *  'id' => (int) category id
     *  'parent_id' => (int) parent category id
     *  'parent_skg' => (string) parent category group code
     * )
     *
     * @var array[]
     */
    protected $oldSkgs = [];
    /**
     * New (that will be create in import) skgs
     *
     * [SKG] => array(
     *  'parent_skg' => (string) parent category skg
     *  'name' => (string) category name
     *  'is_active' => (bool) active flag
     *  'url_key' => (string) category url key
     *  'row_num' => (int) import row number
     * )
     *
     * @var array
     */
    protected $newSkgs = [];

    /**
     * Moved skgs
     *
     * [SKG] => [
     *  'parent_skg' => (string) old parent category skg
     *  'new_parent_skg' => (string) new parent category skg
     *  'row_num' => (int) import row number
     * ]
     *
     * @var array
     */
    protected $movedSkgs = [];
    /**
     * Category group code that is going to be deleted
     *
     * [SKG] => (int) import row number
     *
     * @var array
     */
    protected $deletedSkgs = [];
    /**
     * Old category SKG to ID map
     *
     * @var array
     */
    protected $oldSkgToId = [];
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var CategoryListInterface
     */
    private $categoryList;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * SkgProcessor constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository The category repository
     * @param CategoryListInterface $categoryList The category list
     * @param SearchCriteriaBuilderFactory $criteriaBuilder The search criteria builder factory
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        CategoryListInterface $categoryList,
        SearchCriteriaBuilderFactory $criteriaBuilder
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->categoryList = $categoryList;
        $this->searchCriteriaBuilderFactory = $criteriaBuilder;
    }

    /**
     * Reload existing skg
     *
     * @return SkgProcessor
     * @throws NoSuchEntityException
     */
    public function reloadOldSkgs(): SkgProcessor
    {
        $this->getSkgs();
        return $this;
    }

    /**
     * Load category SKGs from repository
     *
     * @throws NoSuchEntityException
     */
    private function getSkgs()
    {
        $criteria = $this->searchCriteriaBuilderFactory->create()->create();
        foreach ($this->categoryList->getList($criteria)->getItems() as $category) {
            $parentSkg = CategoryGroupCodeInterface::ROOT_PARENT_SKG;
            if ($category->getParentId() != 0) {
                $parent = $this->categoryRepository->get($category->getParentId());
                $parentSkg = $parent->getExtensionAttributes()->getSkg();
            }
            $this->oldSkgs[$category->getExtensionAttributes()->getSkg()] = [
                'id' => $category->getId(),
                'parent_id' => $category->getParentId(),
                'parent_skg' => $parentSkg
            ];
            $this->oldSkgToId[$category->getExtensionAttributes()->getSkg()] = $category->getId();
        }
    }

    /**
     * Dry add new SKG
     *
     * @param string $skg The new skg
     * @param array $data The skg data
     *
     * @return SkgProcessor
     */
    public function addNewSkg(string $skg, array $data): SkgProcessor
    {
        $this->newSkgs[strtolower($skg)] = $data;
        return $this;
    }

    /**
     * Set dry-added skg data
     *
     * @param string $skg The skg
     * @param string $key The skg data key
     * @param array $data The skg data
     *
     * @return SkgProcessor
     */
    public function setNewSkgData(string $skg, string $key, $data): SkgProcessor
    {
        $skg = strtolower($skg);
        if (isset($this->newSkgs[$skg])) {
            $this->newSkgs[$skg][$key] = $data;
        }
        return $this;
    }

    /**
     * Dry-delete skg
     *
     * @param string $skg The skg
     * @param int $rowNum The import row number
     *
     * @return SkgProcessor
     */
    public function deleteSkg(string $skg, int $rowNum): SkgProcessor
    {
        $this->deletedSkgs[strtolower($skg)] = $rowNum;
        return $this;
    }

    /**
     * Dry-move skg from one parent to another
     *
     * @param string $skg The skg
     * @param array $data The skg data
     *
     * @return SkgProcessor
     */
    public function moveSkg(string $skg, array $data): SkgProcessor
    {
        $this->movedSkgs[$skg] = $data;
        return $this;
    }

    /**
     * Set dry-moved skg data
     *
     * @param string $skg The skg
     * @param array $data The skg data
     *
     * @return SkgProcessor
     */
    public function setMovedSkgData(string $skg, array $data): SkgProcessor
    {
        $this->movedSkgs[$skg] = $data;
        return $this;
    }

    /**
     * Get dry-run result
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getResultSkgs(): array
    {
        $resultSkgs = [];
        foreach ($this->getOldSkg() as $key => $value) {
            $resultSkgs[$key] = $value;
        }
        foreach ($this->getNewSkg() as $key => $value) {
            $resultSkgs[$key] = $value;
        }
        $resultSkgs = array_diff_key($resultSkgs, $this->getDeletedSkg());
        foreach ($this->getMovedSkg() as $skg => $data) {
            if (isset($resultSkgs[$skg])) {
                $resultSkgs[$skg]['parent_skg'] = $data['new_parent_skg'];
            }
        }
        return $resultSkgs;
    }

    /**
     * Get old skgs
     *
     * @param string|null $skg
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getOldSkg(?string $skg = null)
    {
        if (!$this->oldSkgs) {
            $this->getSkgs();
        }
        if ($skg !== null) {
            return $this->oldSkgs[strtolower($skg)] ?? null;
        }
        return $this->oldSkgs;
    }

    /**
     * Get new skg data or all new skgs
     *
     * @param string|null $skg
     *
     * @return array|null
     */
    public function getNewSkg(?string $skg = null): ?array
    {
        if ($skg !== null) {
            return $this->newSkgs[strtolower($skg)] ?? null;
        }
        return $this->newSkgs;
    }

    /**
     * Get deleted flag of skg or all deleted skgs
     *
     * @param string|null $skg
     *
     * @return array|int|mixed
     */
    public function getDeletedSkg(?string $skg = null)
    {
        if ($skg !== null) {
            return $this->deletedSkgs[strtolower($skg)] ?? null;
        }
        return $this->deletedSkgs;
    }

    /**
     * Get one moved skg or list of all skgs
     *
     * @param string|null $skg
     *
     * @return array|null
     */
    public function getMovedSkg(?string $skg = null)
    {
        if ($skg !== null) {
            return $this->movedSkgs[$skg] ?? null;
        }
        return $this->movedSkgs;
    }
}
