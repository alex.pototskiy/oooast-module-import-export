<?php
/**
 * PHP version 7.1
 *
 * Category import validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 8:38
 */

declare(strict_types=1);

namespace OooAst\ImportExport\Model\Import\Category;

use Magento\Framework\Validator\AbstractValidator;
use OooAst\ImportExport\Model\Import\Category;
use Zend_Validate_Exception;

/**
 * Class Validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class Validator extends AbstractValidator implements RowValidatorInterface
{

    /** @var AbstractValidator[]|RowValidatorInterface[] */
    protected $validators = [];
    /** @var Category */
    protected $context;
    /** @var string|null */
    protected $invalidAttribute;
    /** @var string[] */
    private $rowData;
    /**
     * Attributes validator
     *
     * @var Validator\Attribute
     */
    private $attribute;

    /**
     * Validator constructor.
     *
     * @param Validator\Attribute $attribute The attributes validator
     * @param RowValidatorInterface[] $validators
     */
    public function __construct(
        Category\Validator\Attribute $attribute,
        $validators = []
    ) {
        $this->attribute = $attribute;
        $this->validators = $validators;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     *
     * @return boolean
     * @throws Zend_Validate_Exception If validation of $value is impossible
     */
    public function isValid(
        $value
    ): bool {
        $this->rowData = $value;
        $this->_clearMessages();
        if (!is_array($value)) {
            $this->_addMessages(
                [
                    $this->context->retrieveMessageTemplate(self::ERROR_ROW_IS_ORPHAN)
                ]
            );
            return false;
        }
        $returnValue = $this->isValidAttributes();
        foreach ($this->validators as $validator) {
            if (!$validator->isValid($value)) {
                $returnValue = false;
                $this->_addMessages($validator->getMessages());
            }
        }
        return $returnValue;
    }

    /**
     * Validate all attributes in data row
     *
     * @return bool
     */
    protected function isValidAttributes()
    {
        $this->_clearMessages();
        $this->setInvalidAttribute(null);
        $categoryTypeModel = $this->context->getCategoryType();
        $rowAction = $this->context->getRowAction($this->rowData);
        if ($categoryTypeModel) {
            foreach (array_keys($this->rowData) as $attrCode) {
                $attrParams = $categoryTypeModel->retrieveAttributeFromCache($attrCode);
                if ($attrParams) {
                    $attr = $this->attribute->isAttributeValid(
                        $this->rowData,
                        $attrCode,
                        $attrParams,
                        $rowAction
                    );
                    if ($attr !== null) {
                        $this->setInvalidAttribute($attr);
                    }
                }
            }
        }
        if ($this->getMessages()) {
            return false;
        }
        return true;
    }

    /**
     * Get stored invalid attribute code
     *
     * @return string|null
     */
    public function getInvalidAttribute(): ?string
    {
        return $this->invalidAttribute;
    }

    /**
     * Store invalid attribute code
     *
     * @param string|null $attribute The attribute code
     */
    protected function setInvalidAttribute(?string $attribute)
    {
        $this->invalidAttribute = $attribute;
    }

    /**
     * Initialize validator
     *
     * @param Category $context The category import model
     *
     * @return RowValidatorInterface
     */
    public function init(Category $context): RowValidatorInterface
    {
        $this->context = $context;
        $this->attribute->init($context);
        foreach ($this->validators as $validator) {
            $validator->init($context);
        }
        return $this;
    }
}
