<?php
/**
 * PHP version 7.1
 *
 * Category import row validator interface
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 8:43
 */

namespace OooAst\ImportExport\Model\Import\Category;

use Magento\Framework\Validator\ValidatorInterface;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category import row data validator interface
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
interface RowValidatorInterface extends ValidatorInterface
{
    const ERROR_INVALID_SCOPE = 'invalidScope';
    const ERROR_INVALID_WEBSITE = 'invalidWebsite';
    const ERROR_INVALID_CATEGORY = 'invalidCategory';
    const ERROR_TRY_DELETE_TWICE = 'tryDeleteTwice';
    const ERROR_VALUE_IS_REQUIRED = 'isRequired';
    const ERROR_SKG_IS_EMPTY = 'skgEmpty';
    const ERROR_NO_DEFAULT_ROW = 'noDefaultRow';
    const ERROR_DUPLICATE_SCOPE = 'duplicateScope';
    const ERROR_DUPLICATE_SKG = 'duplicateSKU';
    const ERROR_ROW_IS_ORPHAN = 'rowIsOrphan';
    const ERROR_SKG_NOT_FOUND_FOR_DELETE = 'skuNotFoundToDelete';
    const ERROR_MEDIA_DATA_INCOMPLETE = 'mediaDataIsIncomplete';
    const ERROR_INVALID_ATTRIBUTE_TYPE = 'invalidAttributeType';
    const ERROR_INVALID_ATTRIBUTE_DECIMAL = 'invalidAttributeDecimal';
    const ERROR_ABSENT_REQUIRED_ATTRIBUTE = 'absentRequiredAttribute';
    const ERROR_INVALID_ATTRIBUTE_OPTION = 'absentAttributeOption';
    const ERROR_DUPLICATE_UNIQUE_ATTRIBUTE = 'duplicatedUniqueAttribute';
    const ERROR_INVALID_VARIATIONS_CUSTOM_OPTIONS = 'invalidVariationsCustomOptions';
    const ERROR_INVALID_MEDIA_URL_OR_PATH = 'invalidMediaUrlPath';
    const ERROR_MEDIA_URL_NOT_ACCESSIBLE = 'mediaUrlNotAvailable';
    const ERROR_MEDIA_PATH_NOT_ACCESSIBLE = 'mediaPathNotAvailable';
    const ERROR_DUPLICATE_URL_KEY = 'duplicatedUrlKey';
    const ERROR_DUPLICATE_MULTISELECT_VALUES = 'duplicatedMultiselectValues';
    const ERROR_EXCEEDED_MAX_LENGTH = 'exceededMaxLength';

    const ERROR_NO_STORE_ROOT_CATEGORY = 'noStoreRootCategory';
    const ERROR_CAN_NOT_SAVE_ENTITY = 'canNotSaveEntity';
    const ERROR_CAN_NOT_MOVE = 'canNotMoveCategory';
    const ERROR_CAN_NOT_DELETE = 'canNotDeleteCategory';
    const ERROR_CATEGORY_ADDED_AND_DELETED = 'addedAndDeleted';
    const ERROR_CATEGORY_DELETED_AND_ADDED = 'deletedAndAdded';
    const ERROR_WRONG_PARENT = 'wrongParentCategory';

    /**
     * Initialize validator
     *
     * @param Category $context
     *
     * @return $this
     */
    public function init(Category $context): RowValidatorInterface;
}
