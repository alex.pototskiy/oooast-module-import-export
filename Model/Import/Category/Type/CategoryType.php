<?php
/**
 * PHP version 7.1
 *
 * Category type representation
 *
 * @category ImportExport
 * @package OooAst_ImportExport
 * @author Alexander Pototskiy <alex@pototskiy.net>
 * @license http://opensource.org/licenses/gpl-license.php GPL
 * @link http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 10:04
 */

namespace OooAst\ImportExport\Model\Import\Category\Type;

use Magento\Catalog\Api\CategoryAttributeRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\ImportExport\Model\Import;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category model type
 *
 * @category ImportExport
 * @package OooAst_ImportExport
 * @author Alexander Pototskiy <alex@pototskiy.net>
 * @license http://opensource.org/licenses/gpl-license.php GPL
 * @link http://oooast-site/admin
 */
class CategoryType
{
    /**
     * Common attributes cache
     *
     * @var array[]
     */
    public static $commonAttrCache = [];

    /**
     * List of invisible attributes
     *
     * @var array
     */
    public static $invAttributesCache = [];
    /**
     * Attribute Code to Id cache
     *
     * @var array
     */
    public static $attributeCodeToId = [];
    /**
     * Attributes' codes which will be allowed anyway, independently from its visibility property.
     *
     * @var array
     */
    protected $forcedAttributeCodes = [];
    /** @var Category */
    protected $entityModel;
    /**
     * Attributes with index (not label) value.
     *
     * @var array
     */
    protected $indexValueAttributes = [];
    /** @var CategoryAttributeRepositoryInterface */
    private $categoryAttrRepo;
    /** @var SearchCriteriaBuilderFactory */
    private $criteriaBuilder;

    /**
     * CategoryType constructor.
     *
     * @param CategoryAttributeRepositoryInterface $categoryAttrRepo The category attribute repository
     * @param SearchCriteriaBuilderFactory $criteriaBuilder The search criteria builder factory
     * @param Category $entityModel The category import model
     */
    public function __construct(
        CategoryAttributeRepositoryInterface $categoryAttrRepo,
        SearchCriteriaBuilderFactory $criteriaBuilder,
        Category $entityModel
    ) {
        $this->entityModel = $entityModel;
        $this->categoryAttrRepo = $categoryAttrRepo;
        $this->criteriaBuilder = $criteriaBuilder;

        $this->initAttributes();
    }

    /**
     * Init category mode type attributes
     */
    protected function initAttributes()
    {
        $entityAttributes = $this->categoryAttrRepo->getList(
            $this->criteriaBuilder->create()->create()
        )->getItems();
        foreach ($entityAttributes as $attribute) {
            if ($attribute->getIsVisible() ||
                in_array($attribute->getAttributeCode(), $this->forcedAttributeCodes)) {
                self::$commonAttrCache[$attribute->getAttributeId()] = [
                    'id' => $attribute->getAttributeId(),
                    'code' => $attribute->getAttributeCode(),
                    'is_required' => $attribute->getIsRequired(),
                    'is_unique' => $attribute->getIsUnique(),
                    'frontend_label' => $attribute->getDefaultFrontendLabel(),
                    'is_static' => $attribute->getBackendType() == 'static',
                    'type' => ($attribute instanceof AbstractAttribute) ?
                        Import::getAttributeType($attribute) :
                        $attribute->getBackendType(),
                    'default_value' => strlen($attribute->getDefaultValue())
                        ? $attribute->getDefaultValue() : null,
                    'options' => $this->entityModel->getAttributeOptions(
                        $attribute,
                        $this->indexValueAttributes
                    )
                ];
                self::$attributeCodeToId[$attribute->getAttributeCode()] =
                    $attribute->getAttributeId();
            }
        }
    }

    /**
     * Get attribute parameters from cache
     *
     * @param string $attributeCode The attribute code
     *
     * @return array
     */
    public function retrieveAttributeFromCache(string $attributeCode)
    {
        if (isset(self::$attributeCodeToId[$attributeCode]) &&
            $attrId = self::$attributeCodeToId[$attributeCode]) {
            if (isset(self::$commonAttrCache[$attrId])) {
                return self::$commonAttrCache[$attrId];
            }
        }
        return [];
    }

    /**
     * Validate row attributes. Pass VALID row data ONLY as argument.
     *
     * @param string[] $rowData The import data row
     * @param int $rowNum The import row number
     * @param bool $isNewCategory The new category flag
     *
     * @return bool
     */
    public function isRowValid(array $rowData, int $rowNum, bool $isNewCategory): bool
    {
        $error = false;
        $rowScope = $this->entityModel->getRowScope($rowData);
        if (!empty($rowData[Category::COL_SKG])) {
            foreach (self::$attributeCodeToId as $attrCode => $attrId) {
                /** @var mixed[] $attrParams */
                $attrParams = self::$commonAttrCache[$attrId];
                if ($this->isNotEmptyAttr($rowData, $attrCode)) {
                    $error |= !$this->entityModel->isAttributeValid(
                        $attrCode,
                        $attrParams,
                        $rowData,
                        $rowNum
                    );
                } elseif ($this->isRequiredButNotValid($rowData, $isNewCategory, $attrCode, $attrParams, $rowScope)) {
                    $error = true;
                    $this->entityModel->addRowError(
                        Category\RowValidatorInterface::ERROR_VALUE_IS_REQUIRED,
                        $rowNum,
                        $attrCode
                    );
                }
            }
        }
        return !$error;
    }

    /**
     * Check if attribute value is not empty
     *
     * @param array $rowData
     * @param string $attrCode
     *
     * @return bool
     */
    private function isNotEmptyAttr(array $rowData, $attrCode): bool
    {
        return isset($rowData[$attrCode]) && strlen($rowData[$attrCode]);
    }

    /**
     * Check if attribute is required and it's value is not valid
     *
     * @param array $rowData
     * @param bool $isNewCategory
     * @param string $attrCode
     * @param array $attrParams
     * @param int $rowScope
     *
     * @return bool
     */
    private function isRequiredButNotValid(
        array $rowData,
        bool $isNewCategory,
        $attrCode,
        array $attrParams,
        int $rowScope
    ): bool {
        return $this->isAttributeRequiredCheckNeeded($attrCode) && $attrParams['is_required']
            && Category::SCOPE_DEFAULT == $rowScope
            && ($isNewCategory || array_key_exists($attrCode, $rowData));
    }

    /**
     * Test if attribute should be validate against 'is_required'
     *
     * @param string $attrCode
     *
     * @return bool
     */
    private function isAttributeRequiredCheckNeeded(
        string $attrCode
    ) {
        return !in_array($attrCode, $this->entityModel->getNullValuesAttributes());
    }

    /**
     * Adding default attribute to product before save.
     *
     * Prepare attributes values for save: exclude non-existent, static or with empty values attributes,
     * set default values if needed.
     *
     * @param array $rowData
     * @param bool $withDefaultValue
     *
     * @return array
     */
    public function prepareAttributesWithDefaultValueForSave(
        array $rowData,
        bool $withDefaultValue = true
    ) {
        $resultAttrs = [];
        $rowScope = $this->entityModel->getRowScope($rowData);
        foreach (self::$attributeCodeToId as $attrCode => $attrId) {
            $attrParams = self::$commonAttrCache[$attrId];
            if ($attrParams['is_static']) {
                continue;
            }
            if ($this->isValueNotEmpty($rowData, $attrCode)) {
                $this->prepareSimpleAndOptionValues(
                    $rowData,
                    $attrParams,
                    (string)$attrCode,
                    $resultAttrs
                );
            } elseif ($this->isValueExitsAndScopeIsDefault($rowData, $attrCode, $rowScope)) {
                $resultAttrs[$attrCode] = $rowData[$attrCode];
            } elseif ($this->doesAssignDefaultValue($withDefaultValue, $attrParams, $rowScope)) {
                $resultAttrs[$attrCode] = $attrParams['default_value'];
            }
        }
        return $resultAttrs;
    }

    /**
     * Check if attribute value is not empty
     *
     * @param array $rowData
     * @param string $attrCode
     *
     * @return bool
     */
    private function isValueNotEmpty(array $rowData, $attrCode): bool
    {
        return isset($rowData[$attrCode]) && strlen(trim($rowData[$attrCode]));
    }

    /**
     * Prepare simple and option attribute value
     *
     * @param array $rowData The import data row
     * @param array $attrParams The attribute parameters
     * @param string $attrCode The attribute code
     * @param array $resultAttrs The result array of attributes
     */
    private function prepareSimpleAndOptionValues(
        array $rowData,
        array $attrParams,
        string $attrCode,
        array &$resultAttrs
    ): void {
        if (in_array($attrParams['type'], ['select', 'boolean'])) {
            $resultAttrs[$attrCode] = $attrParams['options'][strtolower(
                $rowData[$attrCode]
            )];
        } elseif ($attrParams['type'] == 'multiselect') {
            foreach ($this->entityModel->parseMultiselectValues(
                $rowData[$attrCode],
                $this->entityModel->getMultipleValueSeparator()
            ) as $value) {
                $resultAttrs[$attrCode][] = $attrParams['options'][strtolower($value)];
            }
        } else {
            $resultAttrs[$attrCode] = $rowData[$attrCode];
        }
    }

    /**
     * Check if attribute value exists and row scope is default
     *
     * @param array $rowData
     * @param string $attrCode
     * @param int $rowScope
     *
     * @return bool
     */
    private function isValueExitsAndScopeIsDefault(array $rowData, $attrCode, int $rowScope): bool
    {
        return array_key_exists($attrCode, $rowData) && $rowScope == Category::SCOPE_DEFAULT;
    }

    /**
     * Check if default value should be assigned
     *
     * @param bool $withDefaultValue
     * @param array $attrParams
     * @param int $rowScope
     *
     * @return bool
     */
    private function doesAssignDefaultValue(bool $withDefaultValue, array $attrParams, int $rowScope): bool
    {
        return $withDefaultValue && $attrParams['default_value'] !== null
            && $rowScope == Category::SCOPE_DEFAULT;
    }

    /**
     * Clear empty columns in the Row Data
     *
     * @param array $rowData
     *
     * @return array
     */
    public function clearEmptyData(
        array $rowData
    ): array {
        foreach (self::$attributeCodeToId as $attrCode => $attrId) {
            $attrParams = self::$commonAttrCache[$attrId];
            if (!$attrParams['is_static'] && !isset($rowData[$attrCode])) {
                unset($rowData[$attrCode]);
            }
            if (isset($rowData[$attrCode]) &&
                $rowData[$attrCode] === $this->entityModel->getEmptyAttributeValueConstant()) {
                $rowData[$attrCode] = null;
            }
        }
        return $rowData;
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        self::$attributeCodeToId = [];
        self::$commonAttrCache = [];
    }
}
