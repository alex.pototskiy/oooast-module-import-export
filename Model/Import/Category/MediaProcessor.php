<?php
/**
 * PHP version 7.1
 * Category import image processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 26.04.2019
 * Time: 7:57
 */

namespace OooAst\ImportExport\Model\Import\Category;

use Exception;
use Magento\CatalogImportExport\Model\Import\Uploader;
use Magento\CatalogImportExport\Model\Import\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category import image processor
 *
 * @package OooAst\ImportExport\Model\Import\Category
 */
class MediaProcessor
{
    const PATH_REGEXP = '#^(?!.*[\\/]\.{2}[\\/])(?!\.{2}[\\/])[-\w.\\/]+$#';

    const COL_IMAGE = 'image';
    /**
     * Category import model as context
     *
     * @var Category
     */
    private $context;
    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;
    /**
     * URL validator
     *
     * @var \Magento\Framework\Url\Validator
     */
    private $urlValidator;
    /**
     * Image file uploader
     *
     * @var Uploader
     */
    private $uploader;
    /**
     * Magento file system
     *
     * @var Filesystem
     */
    private $filesystem;
    /**
     * Media directory
     *
     * @var Filesystem\Directory\WriteInterface
     */
    private $mediaDir;

    /**
     * MediaProcessor constructor.
     *
     * @param UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Url\Validator $urlValidator
     * @param Filesystem $filesystem
     *
     * @throws FileSystemException
     */
    public function __construct(
        UploaderFactory $uploaderFactory,
        \Magento\Framework\Url\Validator $urlValidator,
        Filesystem $filesystem
    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->urlValidator = $urlValidator;
        $this->filesystem = $filesystem;
        $this->mediaDir = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    /**
     * Init import context
     *
     * @param Category $context
     */
    public function init(Category $context)
    {
        $this->context = $context;
    }

    /**
     * Prepare image attribute
     *
     * @param array $data
     *
     * @throws Exception
     */
    public function prepareImageAttribute(array &$data)
    {
        if (!isset($data[self::COL_IMAGE]) || !strlen($data[self::COL_IMAGE])) {
            $data[self::COL_IMAGE] = '';
        } else {
            $file = $this->getUploader()->move($data[self::COL_IMAGE]);
            $file = $file['file'];
            if (substr($file, 0, 1) == '/') {
                $file = substr($file, 1);
            }
            $data[self::COL_IMAGE] = $file;
        }
    }

    /**
     * Get image file uploader
     *
     * @return Uploader
     * @throws LocalizedException
     */
    private function getUploader()
    {
        if ($this->uploader === null) {
            $this->uploader = $this->uploaderFactory->create();
            $this->uploader->init();

            $dirConfig = DirectoryList::getDefaultConfig();
            $dirAddon = $dirConfig[DirectoryList::MEDIA][DirectoryList::PATH];

            if ($this->context->getImageImportBaseDir() !== null) {
                $tmpPath = $this->context->getImageImportBaseDir();
            } else {
                $tmpPath = $dirAddon . '/' . $this->mediaDir->getRelativePath('import');
            }

            if (!$this->uploader->setTmpDir($tmpPath)) {
                throw new LocalizedException(
                    __('File directory \'%1\' is not readable.', $tmpPath)
                );
            }
            $destinationDir = "catalog/category";
            $destinationPath = $dirAddon . '/' . $this->mediaDir->getRelativePath($destinationDir);

            $this->mediaDir->create($destinationPath);
            if (!$this->uploader->setDestDir($destinationPath)) {
                throw new LocalizedException(
                    __('File directory \'%1\' is not writable.', $destinationPath)
                );
            }
        }
        return $this->uploader;
    }

    /**
     * Get absolute path of image to import
     *
     * @param string $image The absolute file path or url
     *
     * @return string
     */
    public function getImageAbsolutePath(string $image): string
    {
        if ($this->isPath($image)) {
            return $this->context->getImageBaseDir()->getAbsolutePath($image);
        } else {
            return $image;
        }
    }

    /**
     * Check if path is valid
     *
     * @param string $path The local file path
     *
     * @return bool
     */
    public function isPath(string $path): bool
    {
        return preg_match(self::PATH_REGEXP, $path);
    }

    /**
     * Compare current and new image file name
     *
     * @param string|null $currentName The current(saved in DB) file name
     * @param string|null $newName The new file path
     *
     * @return bool
     */
    public function isSameImageName(?string $currentName, ?string $newName): bool
    {
        if ($this->isNullOrEmptyString($currentName) && $this->isNullOrEmptyString($newName)) {
            return true;
        } elseif (!$this->isNullOrEmptyString($currentName) && $this->isNullOrEmptyString($newName)) {
            return false;
        } elseif ($this->isNullOrEmptyString($currentName) && !$this->isNullOrEmptyString($newName)) {
            return false;
        }
        $file = $this->imageIndexedName($newName);
        return $currentName == $file;
    }

    /**
     * Check if string null or empty
     *
     * @param string|null $value
     *
     * @return bool
     */
    private function isNullOrEmptyString(?string $value): bool
    {
        return !isset($value) || !strlen(trim($value));
    }

    /**
     * Create mage like image file name
     *
     * @param string $newName
     *
     * @return string
     */
    private function imageIndexedName(string $newName): string
    {
        if ($this->isUrl($newName)) {
            $url = curl_init($newName);
            $basename = basename(curl_getinfo($url, CURLINFO_EFFECTIVE_URL));
        } else {
            $basename = basename($newName);
        }
        return substr($basename, 0, 1)
            . '/' . substr($basename, 1, 1)
            . '/' . $basename;
    }

    /**
     * Check if parameter is valid url
     *
     * @param string $url
     *
     * @return bool
     */
    public function isUrl(string $url): bool
    {
        return $this->urlValidator->isValid($url);
    }
}
