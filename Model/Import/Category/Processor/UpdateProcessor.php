<?php
/**
 * PHP version 7.1
 *
 * Category update processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 17:24
 */
declare(strict_types=1);

namespace OooAst\ImportExport\Model\Import\Category\Processor;

use Exception;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\Catalog\Api\Data\CategoryGroupCodeInterface;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category import update processor
 *
 * @package OooAst\ImportExport\Model\Import\Category\Processor
 */
class UpdateProcessor extends AbstractProcessor
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Category\MediaProcessor
     */
    private $mediaProcessor;

    /**
     * UpdateProcessor constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param StoreManagerInterface $storeManager
     * @param SearchCriteriaHelper $criteriaHelper
     * @param Category\MediaProcessor $mediaProcessor
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        StoreManagerInterface $storeManager,
        SearchCriteriaHelper $criteriaHelper,
        Category\MediaProcessor $mediaProcessor
    ) {
        $this->storeManager = $storeManager;
        $this->mediaProcessor = $mediaProcessor;
        parent::__construct($categoryRepository, $criteriaHelper);
    }

    /**
     * @inheritDoc
     */
    public function init(Category $context): void
    {
        parent::init($context);
        $this->mediaProcessor->init($context);
    }

    /**
     * Execute processor
     *
     * @param array $entities The category entities [RowNumber => RowData]
     *
     * @return void
     * @throws Exception
     */
    public function execute(array $entities): void
    {
        foreach ($entities as $rowNum => ['__store_id' => $storeId,
                 '__parent_skg' => $parentSkg,
                 '__data_to_update' => $rowData]) {
            /** @var \Magento\Catalog\Model\Category $category */
            $category = $this->categoryRepository->getBySkg($rowData[Category::COL_SKG]);
            $this->mediaProcessor->prepareImageAttribute($rowData);
            $category->addData($rowData);
            /**
             * Category parent
             *
             * @var \Magento\Catalog\Model\Category $parent
             */
            $parent = $this->categoryRepository->getBySkg($parentSkg);
            if ($parent !== null) {
                $category->setStoreId($storeId);
                $this->storeManager->setCurrentStore($storeId);
                if ($this->saveWithErrorMessage($category, $rowNum) == null) {
                    continue;
                };
                if ($category->getParentId() != $parent->getId()) {
                    $this->moveWithErrorMessage($category, $parent, $rowNum);
                }
            } elseif ($rowData[Category::COL_PARENT_SKG] != CategoryGroupCodeInterface::ROOT_PARENT_SKG) {
                $this->addRowError(
                    Category::ERROR_PARENT_NOT_FOUND,
                    $rowNum,
                    $rowData[Category::COL_PARENT_SKG]
                );
            }
        }

        return;
    }

    /**
     * Move category to new parent with error message
     *
     * @param \Magento\Catalog\Model\Category $category
     * @param \Magento\Catalog\Model\Category $parent
     * @param int $rowNum
     */
    private function moveWithErrorMessage(
        \Magento\Catalog\Model\Category $category,
        \Magento\Catalog\Model\Category $parent,
        int $rowNum
    ): void {
        if (!$this->categoryRepository->move((int)$category->getId(), (int)$parent->getId())) {
            $this->addRowError(Category::ERROR_CAN_NOT_MOVE, $rowNum);
        };
    }
}
