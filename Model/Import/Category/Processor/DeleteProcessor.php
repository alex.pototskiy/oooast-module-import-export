<?php
/**
 * PHP version 7.1
 *
 * Category delete processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 17:24
 */

namespace OooAst\ImportExport\Model\Import\Category\Processor;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category import delete processor
 *
 * @package OooAst\ImportExport\Model\Import\Category\Processor
 */
class DeleteProcessor extends AbstractProcessor
{
    /**
     * Execute processor
     *
     * @param array $entities The category entities [RowNumber => RowData]
     *
     * @return void
     */
    public function execute(array $entities): void
    {
        foreach ($entities as $rowNum => ['__store_id' => $storeId, '__skg_to_delete' => $skg]) {
            $category = $this->categoryRepository->getBySkg($skg);
            if ($category !== null) {
                $this->moveChildrenToParent($category);
                $this->deleteWithErrorMessage($category, (int)$rowNum);
            } else {
                $this->errorDoesNotExist((string)$skg, (int)$rowNum);
            }
        }
        return;
    }

    /**
     * Remove children of category
     *
     * @param CategoryInterface $category
     */
    private function moveChildrenToParent(CategoryInterface $category)
    {
        $criteria = $this->criteriaHelper->createParentCriteria((int)$category->getId());
        $children = $this->categoryRepository->getList($criteria);
        foreach ($children as $child) {
            $this->categoryRepository->move($child->getId(), $category->getParentId());
        }
    }

    /**
     * Delete category with error message
     *
     * @param CategoryInterface $category
     * @param int $rowNum
     */
    private function deleteWithErrorMessage(CategoryInterface $category, int $rowNum)
    {
        try {
            $this->categoryRepository->delete($category);
        } catch (InputException|NoSuchEntityException|StateException $e) {
            $this->errorDoesNotExist($category->getExtensionAttributes()->getSkg(), $rowNum);
        }
    }

    /**
     * Add error message 'can not delete'
     *
     * @param string $skg
     * @param int $rowNum
     */
    private function errorDoesNotExist(string $skg, int $rowNum)
    {
        $this->addRowError(
            Category::ERROR_CAN_NOT_DELETE,
            $rowNum,
            $skg
        );
    }
}
