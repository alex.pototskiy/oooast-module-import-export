<?php
/**
 * PHP version 7.1
 *
 * Category create processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 17:24
 */

namespace OooAst\ImportExport\Model\Import\Category\Processor;

use Exception;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Category import create processor
 *
 * @package OooAst\ImportExport\Model\Import\Category\Processor
 */
class CreateProcessor extends AbstractProcessor
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * Image processor
     *
     * @var Category\MediaProcessor
     */
    private $mediaProcessor;

    /**
     * CreateProcessor constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param SearchCriteriaHelper $criteriaHelper
     * @param StoreManagerInterface $storeManager
     * @param Category\MediaProcessor $mediaProcessor
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        SearchCriteriaHelper $criteriaHelper,
        StoreManagerInterface $storeManager,
        Category\MediaProcessor $mediaProcessor
    ) {
        $this->storeManager = $storeManager;
        $this->mediaProcessor = $mediaProcessor;
        parent::__construct($categoryRepository, $criteriaHelper);
    }

    /**
     * @inheritDoc
     */
    public function init(Category $context)
    {
        parent::init($context);
        $this->mediaProcessor->init($context);
    }

    /**
     * Execute processor
     *
     * @param array $entities The category entities [RowNumber => RowData]
     *
     * @return void
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(array $entities): void
    {
        foreach ($entities as
                 $rowNum => ['__store_id' => $storeId, '__parent_skg' => $parentSkg, '__data_to_create' => $data]) {
            $criteria = $this->createSkgCriteria($data[Category::COL_SKG]);
            /** @var \Magento\Catalog\Model\Category $category */
            $category = $this->categoryRepository->getBySearchCriteria($criteria);
            if (!$category) {
                $category = $this->categoryRepository->create();
            } else {
                $category = $this->categoryRepository->get($category->getId(), $storeId);
            }
            $data = array_intersect_key(
                $data,
                array_flip(
                    array_map(
                        function (Attribute $val) {
                            return $val->getAttributeCode();
                        },
                        $this->getAttributeCollection()->getItems()
                    )
                )
            );
            $this->mediaProcessor->prepareImageAttribute($data);
            $category->addData($data);
            $category->getExtensionAttributes()->setSkg($category->getData(Category::COL_SKG));
            /** @var \Magento\Catalog\Model\Category $parent */
            $parent = $this->getOrCreateParent($parentSkg);
            if ($category->getId() === null) {
                $category->setParentId($parent->getId());
            }
            $category->setStoreId($storeId);
            $this->storeManager->setCurrentStore($storeId);
            if ($this->saveWithErrorMessage($category, $rowNum) == null) {
                continue;
            };
        }
    }
}
