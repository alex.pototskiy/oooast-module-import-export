<?php
/**
 * PHP version 7.1
 *
 * Category import abstract processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 17:17
 */

namespace OooAst\ImportExport\Model\Import\Category\Processor;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\CouldNotSaveException as CouldNotSaveExceptionAlias;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;
use OooAst\ImportExport\Model\Import\Category;
use OooAst\ImportExport\Model\Import\Category\CategoryProcessorInterface;

/**
 * Abstract category import processor
 *
 * @package OooAst\ImportExport\Model\Import\Category\Processor
 */
abstract class AbstractProcessor implements CategoryProcessorInterface
{
    /**
     * Category import context model
     *
     * @var Category
     */
    protected $context;
    /**
     * Category repository
     *
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;
    /**
     * Import SKG processor
     *
     * @var Category\SkgProcessor
     */
    protected $skgProcessor;
    /**
     * Search criteria helper
     *
     * @var SearchCriteriaHelper
     */
    protected $criteriaHelper;

    /**
     * AbstractProcessor constructor.
     *
     * @param CategoryRepository $categoryRepository
     * @param SearchCriteriaHelper $criteriaHelper
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        SearchCriteriaHelper $criteriaHelper
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->criteriaHelper = $criteriaHelper;
    }

    /**
     * Init processor
     *
     * @param Category $context The category import model
     */
    public function init(Category $context)
    {
        $this->context = $context;
        $this->skgProcessor = $context->getSkgProcessor();
    }

    /**
     * Add error with corresponding current data source row number.
     *
     * @param string $errorCode Error code or simply column name
     * @param int $errorRowNum Row number.
     * @param string $colName OPTIONAL Column name.
     * @param string $errorMessage OPTIONAL Column name.
     * @param string $errorLevel
     * @param string $errorDescription
     *
     * @return $this
     */
    public function addRowError(
        $errorCode,
        $errorRowNum,
        $colName = null,
        $errorMessage = null,
        $errorLevel = ProcessingError::ERROR_LEVEL_CRITICAL,
        $errorDescription = null
    ) {
        $this->context->addRowError(
            $errorCode,
            $errorRowNum,
            $colName,
            $errorMessage,
            $errorLevel,
            $errorDescription
        );
        return $this;
    }

    /**
     * Get import row scope
     *
     * Delegated to {@link Category::getRowScope}
     *
     * @param array $rowData
     *
     * @return int
     */
    protected function getRowScope(array $rowData)
    {
        return $this->context->getRowScope($rowData);
    }

    /**
     * Get category import category
     *
     * Delegate to {@link Category::getAttributeCollection}
     *
     * @return Collection
     */
    protected function getAttributeCollection(): Collection
    {
        return $this->context->getAttributeCollection();
    }

    /**
     * Get (or create) parent category
     *
     * @param string $parentSkg The parent category SKG
     *
     * @return CategoryInterface
     * @throws NoSuchEntityException
     */
    protected function getOrCreateParent(string $parentSkg): CategoryInterface
    {
        $criteria = $this->createSkgCriteria($parentSkg);
        $category = $this->categoryRepository->getBySearchCriteria($criteria);
        if ($category) {
            return $category;
        } else {
            $nextParent = $this->skgProcessor->getResultSkgs()[$parentSkg]['parent_skg'];
            $parent = $this->getOrCreateParent($nextParent);
            $newCategory = $this->categoryRepository->create();
            $newData = [];
            if (isset($this->skgProcessor->getResultSkgs()[$parentSkg])) {
                $newData = $this->skgProcessor->getResultSkgs()[$parentSkg];
            }
            $newCategory->setName($newData['name']);
            $newCategory->setParentId($parent->getId());
            $newCategory->setIsActive($newData['is_active']);
            if ($newCategory instanceof \Magento\Catalog\Model\Category) {
                $newCategory->getExtensionAttributes()->setSkg($parentSkg);
                $newCategory->setUrlKey($newData['url_key']);
                $newCategory->setStoreId(0);
            }
            return $this->saveWithErrorMessage($newCategory, $newData['row_num']);
        }
    }

    /**
     * Create skg search criteria
     *
     * @param string $skg
     *
     * @return SearchCriteria
     */
    protected function createSkgCriteria(string $skg): SearchCriteria
    {
        return $this->criteriaHelper->createSkgCriteria($skg);
    }

    /**
     * Save category with error message
     *
     * @param CategoryInterface $category The category
     * @param int $rowNum The import row number
     *
     * @return CategoryInterface|null
     */
    protected function saveWithErrorMessage(CategoryInterface $category, int $rowNum): ?CategoryInterface
    {
        try {
            return $this->categoryRepository->save($category);
        } catch (CouldNotSaveExceptionAlias $e) {
            $this->context->addRowError(
                Category::ERROR_CAN_NOT_SAVE_ENTITY,
                $rowNum,
                $e->getMessage()
            );
            return null;
        }
    }
}
