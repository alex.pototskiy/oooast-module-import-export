<?php
/**
 * PHP version 7.1
 *
 * Category import processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 17:10
 */

namespace OooAst\ImportExport\Model\Import\Category;

use OooAst\ImportExport\Model\Import\Category;

/**
 * Category processor interface to make changes in repo
 *
 * @package OooAst\ImportExport\Model\Import\Category
 */
interface CategoryProcessorInterface
{
    /**
     * Execute processor
     *
     * @param array $entities The category entities to process array format is depended of processor type
     *
     * @return mixed
     */
    public function execute(array $entities);

    /**
     * Init import context
     *
     * @param Category $context The category import model
     *
     * @return mixed
     */
    public function init(Category $context);
}
