<?php
/**
 * PHP version 7.1
 *
 * Category import website validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 12:39
 */

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use Magento\Framework\Exception\NoSuchEntityException;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Class CanBeDeleted validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class CanBeDeleted extends AbstractImportValidator implements RowValidatorInterface
{
    /**
     * Import data row
     *
     * @var array
     */
    private $rowData;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value The import data row
     *
     * @return boolean
     * @throws NoSuchEntityException
     */
    public function isValid($value)
    {
        $this->_clearMessages();
        $result = true;
        $this->rowData = $value;
        $skgProcessor = $this->context->getSkgProcessor();
        $rowAction = $this->context->getRowAction($this->rowData);
        if ($rowAction != $this->context::ACTION_DELETE) {
            return true;
        }
        $skg = $this->rowData[$this->context::COL_SKG];
        if (!isset($skgProcessor->getOldSkg()[$skg])) {
            $this->_addMessages(
                [
                    $this->context->retrieveMessageTemplate(
                        RowValidatorInterface::ERROR_INVALID_CATEGORY
                    )
                ]
            );
            $result = false;
        }
        if (isset($skgProcessor->getDeletedSkg()[$skg])) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_TRY_DELETE_TWICE
                        ),
                        $skg
                    )
                ]
            );
            $result = false;
        }

        return $result;
    }
}
