<?php
/**
 * PHP version 7.1
 *
 * Category import attribute validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 13:21
 */

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use IntlDateFormatter;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Stdlib\StringUtils;
use OooAst\ImportExport\Model\Import\Category;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Category attribute validator
 *
 * @package OooAst_ImportExport
 */
class Attribute extends AbstractImportValidator implements RowValidatorInterface
{
    /**
     * Import data row
     *
     * @var array
     */
    private $rowData;
    /**
     * Keep unique attributes to detect unique violence
     *
     * @var array
     */
    private $uniqueAttributes;
    /**
     * @var StringUtils
     */
    private $string;
    /**
     * @var ResolverInterface
     */
    private $localeResolver;
    /**
     * Attributes with type: datetime that contains date+time, other contains only date
     *
     * @var array
     */
    private $datetimeAttributes = [];

    /**
     * Attribute constructor.
     *
     * @param StringUtils $string The string utils
     * @param ResolverInterface $localeResolver The locale helper
     */
    public function __construct(
        StringUtils $string,
        ResolverInterface $localeResolver
    ) {
        $this->string = $string;
        $this->localeResolver = $localeResolver;
    }

    /**
     * Do not use, call isAttributeValid instead.
     *
     * @param array $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function isValid($value): bool
    {
        return true;
    }

    /**
     * Test if attribute is valid
     *
     * @param array $rowData The import row data
     * @param string $attrCode The attribute code
     * @param array $attrParams The attribute parameters
     * @param string $rowAction The row import action
     *
     * @return string|null The invalid attribute name or null - attribute is valid
     */
    public function isAttributeValid(
        array $rowData,
        string $attrCode,
        array $attrParams,
        string $rowAction
    ): ?string {
        $this->rowData = $rowData;
        if (!$this->isRequiredAttributeValid($attrCode, $attrParams, $rowAction)) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(self::ERROR_VALUE_IS_REQUIRED),
                        $attrCode
                    )
                ]
            );
            return false;
        }
        if (!strlen(trim($this->rowData[$attrCode]))) {
            return true;
        }
        $valid = $this->isTypeValid($attrCode, $attrParams);

        if ($valid && !empty($attrParams['is_unique']) && $attrParams['is_unique']) {
            if (isset($this->uniqueAttributes[$attrCode][$this->rowData[$attrCode]])
                && $this->uniqueAttributes[$attrCode][$this->rowData[$attrCode]] != $this->rowData[Category::COL_SKG]) {
                $this->_addMessages(
                    [
                        sprintf(
                            $this->context->retrieveMessageTemplate(
                                RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE
                            ),
                            $attrCode
                        )
                    ]
                );
                return $attrCode;
            }
            $this->uniqueAttributes[$attrCode][$this->rowData[$attrCode]] = $this->rowData[Category::COL_SKG];
        }

        return $valid ? null : $attrCode;
    }

    /**
     * Check that required attribute is valid
     *
     * @param string $attrCode The attribute code
     * @param array $attrParams The attribute parameters
     * @param string $rowAction The row import action
     *
     * @return bool
     */
    protected function isRequiredAttributeValid(
        string $attrCode,
        array $attrParams,
        string $rowAction
    ): bool {
        $rowScope = $this->context->getRowScope($this->rowData);
        $doCheck = ($attrCode == Category::COL_SKG)
            || ($attrParams['is_required']
                && $rowAction != Category::ACTION_DELETE
                && $rowScope == Category::SCOPE_DEFAULT
                && !in_array($attrCode, $this->context->getNullValuesAttributes()));
        if ($doCheck === true) {
            return isset($this->rowData[$attrCode])
                && strlen(trim($this->rowData[$attrCode]))
                && trim(
                    $this->rowData[$attrCode]
                ) != $this->context->getEmptyAttributeValueConstant();
        }
        return true;
    }

    /**
     * Check that value is valid type value
     *
     * @param string $attrCode The attribute code
     * @param array $attrParams The attribute parameters
     *
     * @return bool
     */
    private function isTypeValid(
        string $attrCode,
        array $attrParams
    ): bool {
        switch ($attrParams['type']) {
            case 'varchar':
            case 'text':
                $valid = $this->textValidation($attrCode, $attrParams['type']);
                break;
            case 'decimal':
            case 'int':
                $valid = $this->numericValidation($attrCode, $attrParams['type']);
                break;
            case 'select':
            case 'boolean':
                $valid = $this->validateOption(
                    $attrCode,
                    $attrParams['options'],
                    $this->rowData[$attrCode]
                );
                break;
            case 'multiselect':
                $valid = $this->isMultiselectValid($attrCode, $attrParams);
                break;
            case 'datetime':
                $valid = $this->datetimeValidation($attrCode, $this->rowData[$attrCode]);
                break;
            default:
                $valid = true;
                break;
        }
        return $valid;
    }

    /**
     * Check that attribute value is valid text or varchar value
     *
     * @param string $attrCode The attribute code
     * @param string $type The attribute backend type
     *
     * @return bool
     */
    private function textValidation(
        string $attrCode,
        string $type
    ) {
        $val = $this->string->cleanString($this->rowData[$attrCode]);
        if ($type == 'text') {
            $valid = $this->string->strlen($val) <= Category::DB_MAX_TEXT_LENGTH;
        } elseif ($attrCode == Category::COL_SKG) {
            $valid = $this->string->strlen($val) <= Category::SKG_MAX_LEN;
        } else {
            $valid = $this->string->strlen($val) <= Category::DB_MAX_VARCHAR_LENGTH;
        }
        if (!$valid) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_EXCEEDED_MAX_LENGTH
                        ),
                        $attrCode
                    )
                ]
            );
        }
        return $valid;
    }

    /**
     * Check that value is correct presentation of numeric value
     *
     * @param string $attrCode The attribute code
     * @param string $type The attribute type
     *
     * @return bool
     */
    private function numericValidation(
        string $attrCode,
        $type
    ) {
        $val = trim($this->rowData[$attrCode]);
        if ($type == 'int') {
            $valid = (string)(int)$val === $val;
        } else {
            $valid = is_numeric($val);
        }
        if (!$valid) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_INVALID_ATTRIBUTE_TYPE
                        ),
                        $attrCode,
                        $type
                    )
                ]
            );
        }
        return $valid;
    }

    /**
     * Validate attribute with options
     *
     * @param string $attrCode The attribute code
     * @param array $options The attribute options
     * @param string $value The attribute value
     *
     * @return bool
     */
    private function validateOption(
        string $attrCode,
        array $options,
        string $value
    ) {
        if (!isset($options[strtolower($value)])) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_INVALID_ATTRIBUTE_OPTION
                        ),
                        $attrCode
                    )
                ]
            );
            return false;
        }
        return true;
    }

    /**
     * Validate that value is correct date or date-time string
     *
     * @param string $attrCode The attribute code
     * @param string $value The attribute value
     *
     * @return bool
     */
    private function datetimeValidation(
        string $attrCode,
        string $value
    ) {
        $val = trim($value);
        /** @noinspection PhpComposerExtensionStubsInspection */
        $format = in_array($attrCode, $this->datetimeAttributes)
            ? IntlDateFormatter::create(
                $this->localeResolver->getDefaultLocale(),
                IntlDateFormatter::SHORT,
                IntlDateFormatter::SHORT
            )
            :
            IntlDateFormatter::create(
                $this->localeResolver->getDefaultLocale(),
                IntlDateFormatter::SHORT,
                IntlDateFormatter::NONE
            );
        if (!$format->parse($val)) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_INVALID_ATTRIBUTE_TYPE
                        ),
                        $attrCode
                    )
                ]
            );
            return false;
        }
        return true;
    }

    /**
     * Check if multiselect value is valid
     *
     * @param string $attrCode The attribute code
     * @param array $attrParams The attribute parameters
     *
     * @return bool
     */
    private function isMultiselectValid(string $attrCode, array $attrParams): bool
    {
        $valid = true;
        $values = $this->context->parseMultiselectValues(
            $this->rowData[$attrCode],
            $this->context->getMultipleValueSeparator()
        );
        foreach ($values as $value) {
            $valid = $this->validateOption(
                $attrCode,
                $attrParams['options'],
                $value
            );
            if (!$valid) {
                break;
            }
        }
        $uniqueValues = array_unique($values);
        if (count($uniqueValues) != count($values)) {
            $valid = false;
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->retrieveMessageTemplate(
                            RowValidatorInterface::ERROR_DUPLICATE_MULTISELECT_VALUES
                        ),
                        $attrCode
                    )
                ]
            );
        }
        return $valid;
    }
}
