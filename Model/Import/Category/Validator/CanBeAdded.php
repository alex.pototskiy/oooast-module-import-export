<?php
/**
 * PHP version 7.1
 * Category import add possibility validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 20.04.2019
 * Time: 10:02
 */

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use Magento\Framework\Exception\NoSuchEntityException;
use OooAst\ImportExport\Model\Import\Category;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Class CanBeAdded validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class CanBeAdded extends AbstractImportValidator implements RowValidatorInterface
{

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value The import data row
     *
     * @return boolean
     * @throws NoSuchEntityException
     */
    public function isValid($value)
    {
        $this->_clearMessages();
        $valid = true;
        $rowAction = $this->context->getRowAction($value);
        if ($rowAction == Category::ACTION_ADD) {
            $skg = $value[Category::COL_SKG];
            $skgProcessor = $this->context->getSkgProcessor();
            if (isset($skgProcessor->getOldSkg()[$skg])) {
                $this->_addMessages(
                    [
                        $this->context
                            ->retrieveMessageTemplate(self::ERROR_DUPLICATE_SKG)
                    ]
                );
                $valid = false;
            }
        }
        return $valid;
    }
}
