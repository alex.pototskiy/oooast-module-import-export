<?php
/**
 * PHP version 7.1
 *
 * Category import abstract validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 17.04.2019
 * Time: 9:05
 */

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use Magento\Framework\Validator\AbstractValidator;
use OooAst\ImportExport\Model\Import\Category;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Class AbstractImportValidator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
abstract class AbstractImportValidator extends AbstractValidator implements RowValidatorInterface
{

    /**
     * Category import model
     *
     * @var Category
     */
    protected $context;

    /**
     * Initialize validator
     *
     * @param Category $context The category import model
     *
     * @return $this
     */
    public function init(Category $context): RowValidatorInterface
    {
        $this->context = $context;
        return $this;
    }
}
