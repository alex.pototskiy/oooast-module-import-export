<?php
/**
 * PHP version 7.1
 *
 * Category import website validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-stie/admin
 * Date: 19.04.2019
 * Time: 17:51
 */

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use OooAst\ImportExport\Model\Export\Category;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Category import website validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-stie/admin
 */
class Website extends AbstractImportValidator implements RowValidatorInterface
{

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     *
     * @return boolean
     */
    public function isValid($value)
    {
        $this->_clearMessages();
        if (empty($value[Category::COL_WEBSITE])) {
            return true;
        }
        $separator = $this->context->getMultipleValueSeparator();
        $websites = explode($separator, $value[Category::COL_WEBSITE]);
        $storeResolver = $this->context->getStoreResolver();
        foreach ($websites as $website) {
            if (!$storeResolver->getWebsiteCodeToId($website)) {
                $this->_addMessages(
                    [
                        sprintf(
                            $this->context->retrieveMessageTemplate(self::ERROR_INVALID_WEBSITE),
                            $website
                        )
                    ]
                );
                return false;
            }
        }
        return true;
    }
}
