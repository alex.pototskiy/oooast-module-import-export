<?php
/**
 * PHP version 7.1
 *
 * Category import media validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 20.04.2019
 * Time: 8:03
 */
declare(strict_types=1);

namespace OooAst\ImportExport\Model\Import\Category\Validator;

use Magento\Catalog\Model\Category;
use Magento\Framework\Filesystem;
use Magento\Framework\Url\Validator;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Import\Category\MediaProcessor;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;

/**
 * Category import media field validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class Media extends AbstractImportValidator implements RowValidatorInterface
{
    const PATH_REGEXP = '#^(?!.*[\\/]\.{2}[\\/])(?!\.{2}[\\/])[-\w.\\/]+$#';

    /** @var array */
    protected $mediaAttributes = ['image'];
    /**
     * Url validator
     *
     * @var Validator
     */
    private $urlValidator;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * Category repository
     *
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * Media processor
     *
     * @var MediaProcessor
     */
    private $mediaProcessor;
    /**
     * Import row data
     *
     * @var array
     */
    private $rowData;

    /**
     * Media constructor.
     *
     * @param Validator $urlValidator The URL validator
     * @param Filesystem $filesystem The Magento file system
     * @param CategoryRepository $categoryRepository
     * @param MediaProcessor $mediaProcessor
     */
    public function __construct(
        Validator $urlValidator,
        Filesystem $filesystem,
        CategoryRepository $categoryRepository,
        MediaProcessor $mediaProcessor
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->urlValidator = $urlValidator;
        $this->filesystem = $filesystem;
        $this->mediaProcessor = $mediaProcessor;
    }

    /**
     * @inheritDoc
     */
    public function init(\OooAst\ImportExport\Model\Import\Category $context): RowValidatorInterface
    {
        parent::init($context);
        $this->mediaProcessor->init($context);
        return $this;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function isValid($value): bool
    {
        $this->rowData = $value;

        $this->_clearMessages();
        if ($this->isImageAlreadyAssigned()) {
            return true;
        }
        $valid = true;
        foreach ($this->mediaAttributes as $attribute) {
            if (isset($this->rowData[$attribute]) && strlen($this->rowData[$attribute])) {
                $attrValue = $this->rowData[$attribute];
                if (!$this->mediaProcessor->isPath($attrValue)
                    && !$this->mediaProcessor->isUrl($attrValue)) {
                    $this->_addMessages(
                        [
                            sprintf(
                                $this->context->retrieveMessageTemplate(
                                    self::ERROR_INVALID_MEDIA_URL_OR_PATH
                                ),
                                $attribute
                            )
                        ]
                    );
                    $valid = false;
                }
                if ($this->mediaProcessor->isPath($attrValue)
                    && !$this->context->getImageBaseDir()->isExist($attrValue)) {
                    $this->_addMessages(
                        [
                            sprintf(
                                $this->context->retrieveMessageTemplate(self::ERROR_INVALID_MEDIA_URL_OR_PATH),
                                $attribute
                            )
                        ]
                    );
                    $valid = false;
                }
            }
        }

        return $valid;
    }

    /**
     * Check if category already has the same image
     *
     * @return bool
     */
    private function isImageAlreadyAssigned(): bool
    {
        $storeId = $this->context->getRowStore($this->rowData);
        $category = $this->categoryRepository->getBySkg($this->rowData['skg'], $storeId);
        if ($category == null) {
            return false;
        } elseif ($category instanceof Category && isset($this->rowData['image'])) {
            return $category->getData('image') == $this->rowData['image'];
        }
        return false;
    }
}
