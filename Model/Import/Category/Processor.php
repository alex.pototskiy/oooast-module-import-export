<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 *
 * Category import processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.04.2019
 * Time: 18:11
 */

namespace OooAst\ImportExport\Model\Import\Category;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;
use OooAst\ImportExport\Model\Import\Category;
use OooAst\ImportExport\Model\Traits\Category\ConstantsInterface;

/**
 * Class Processor
 *
 * @package OooAst\ImportExport\Model\Import\Category
 */
class Processor implements ConstantsInterface
{
    /**
     * Processors
     *
     * @var CategoryProcessorInterface[]
     */
    private $processors;
    /**
     * Import context {@link Category}
     *
     * @var Category
     */
    private $context;
    /**
     * Search criteria helper
     *
     * @var SearchCriteriaHelper
     */
    private $criteriaHelper;
    /**
     * Category repository
     *
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * Media processor
     *
     * @var MediaProcessor
     */
    private $mediaProcessor;

    /**
     * Processor constructor.
     *
     * @param SearchCriteriaHelper $criteriaHelper
     * @param CategoryRepository $categoryRepository
     * @param MediaProcessor $mediaProcessor
     * @param CategoryProcessorInterface[] $processors
     */
    public function __construct(
        SearchCriteriaHelper $criteriaHelper,
        CategoryRepository $categoryRepository,
        MediaProcessor $mediaProcessor,
        $processors
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->criteriaHelper = $criteriaHelper;
        $this->mediaProcessor = $mediaProcessor;
        $this->processors = $processors;
    }

    /**
     * Create categories
     *
     * @param array $entities
     */
    public function createCategories(array $entities)
    {
        $this->processors['create']->execute($entities);
    }

    /**
     * Update categories
     *
     * @param array $entities
     */
    public function updateCategories(array $entities)
    {
        $this->processors['update']->execute($entities);
    }

    /**
     * Delete categories
     *
     * @param array $entities
     */
    public function deleteCategories(array $entities)
    {
        $this->processors['delete']->execute($entities);
    }

    /**
     * Check if category update is needed
     *
     * @param array $data
     * @param string $parentSkg
     * @param int $storeId
     *
     * @return bool true - need to update, false - nothing to update
     * @throws NoSuchEntityException
     */
    public function doUpdate(array $data, string $parentSkg, int $storeId): bool
    {
        $doUpdate = false;
        $skg = $data[Category::COL_SKG];
        $category = $this->categoryRepository->getBySkg($skg);
        if ($category !== null) {
            $category = $this->categoryRepository->get((int)$category->getId(), $storeId);
            if ($category && $category instanceof \Magento\Catalog\Model\Category) {
                foreach ($data as $key => $value) {
                    $oldValue = $category->getData($key);
                    if ($key != 'image' && $oldValue != $value) {
                        $doUpdate = true;
                        break;
                    } elseif ($key == 'image') {
                        $doUpdate = !$this->mediaProcessor->isSameImageName($oldValue, $value);
                        if ($doUpdate == true) {
                            break;
                        }
                    }
                }
                if ($category->getParentCategory()->getExtensionAttributes()->getSkg() != $parentSkg) {
                    $doUpdate = true;
                }
            }
        }

        return $doUpdate;
    }

    /**
     * Init import context
     *
     * @param Category $context
     */
    public function init(Category $context)
    {
        $this->context = $context;
        foreach ($this->processors as $processor) {
            $processor->init($context);
        }
    }
}
