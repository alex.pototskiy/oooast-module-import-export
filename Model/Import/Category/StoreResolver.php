<?php
/**
 * PHP version 7.1
 *
 * Category import store resolver support
 *
 * @category ImportExport
 * @package OooAst_ImportExport
 * @author Alexander Pototskiy <alex@pototskiy.net>
 * @license http://opensource.org/licenses/gpl-license.php GPL
 * @link http://oooast-stie/admin
 * Date: 19.04.2019
 * Time: 17:18
 */

namespace OooAst\ImportExport\Model\Import\Category;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Website;

/**
 * Store resolver class
 *
 * @category ImportExport
 * @package OooAst_ImportExport
 * @author Alexander Pototskiy <alex@pototskiy.net>
 * @license http://opensource.org/licenses/gpl-license.php GPL
 * @link http://oooast-stie/admin
 */
class StoreResolver
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * Website code to id
     *
     * @var array
     */
    private $websiteCodeToId = [];
    /**
     * Website code to store ids
     *
     * @var array
     */
    private $websiteCodeToStoreIds = [];
    /**
     * Store code to id
     *
     * @var array
     */
    private $storeCodeToId = [];
    /**
     * Store code to website store ids
     *
     * @var array
     */
    private $storeIdToWebsiteStoreIds = [];

    /**
     * StoreResolver constructor.
     *
     * @param StoreManagerInterface $storeManager The store manager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Get website id or all websites id
     *
     * @param string|null $code
     *
     * @return array|int|null
     */
    public function getWebsiteCodeToId(?string $code = null)
    {
        if (empty($this->websiteCodeToId)) {
            $this->initWebsites();
        }
        if ($code) {
            return $this->websiteCodeToId[$code] ?? null;
        }
        return $this->websiteCodeToId;
    }

    /**
     * Collect and init websites information
     *
     * @return $this
     */
    protected function initWebsites()
    {
        /** @var Website $website */
        foreach ($this->storeManager->getWebsites() as $website) {
            $this->websiteCodeToId[$website->getCode()] = $website->getId();
            $this->websiteCodeToStoreIds[$website->getCode()] =
                array_flip($website->getStoreCodes());
        }
        return $this;
    }

    /**
     * Get one website store ids or all websites store ids
     *
     * @param string|null $code
     *
     * @return array|null
     */
    public function getWebsiteCodeToStoreIds(?string $code = null)
    {
        if (empty($this->websiteCodeToStoreIds)) {
            $this->initWebsites();
        }
        if ($code) {
            return $this->websiteCodeToStoreIds[$code] ?? null;
        }
        return $this->websiteCodeToStoreIds;
    }

    /**
     * Get store id or all stores id
     *
     * @param string|null $code
     *
     * @return array|int|null
     */
    public function getStoreCodeToId(?string $code = null)
    {
        if (empty($this->storeCodeToId)) {
            $this->initStores();
        }
        if ($code) {
            return $this->storeCodeToId[$code] ?? null;
        }
        return $this->storeCodeToId;
    }

    /**
     * Collect and init stores information
     */
    protected function initStores()
    {
        /** @var Store $store */
        foreach ($this->storeManager->getStores() as $store) {
            $this->storeCodeToId[$store->getCode()] = $store->getId();
            try {
                $this->storeIdToWebsiteStoreIds[$store->getCode()] =
                    $store->getWebsite()->getStoreIds();
            } catch (NoSuchEntityException $e) {
                $this->storeIdToWebsiteStoreIds[$store->getCode()] = [];
            }
        }
    }

    /**
     * Get one store website store ids or the same for all
     *
     * @param string|null $code
     *
     * @return array|mixed|null
     */
    public function getStoreIdToWebsiteStoreIds(?string $code = null)
    {
        if (empty($this->storeIdToWebsiteStoreIds)) {
            $this->initStores();
        }
        if ($code) {
            return $this->storeIdToWebsiteStoreIds[$code] ?? null;
        }
        return $this->storeIdToWebsiteStoreIds;
    }
}
