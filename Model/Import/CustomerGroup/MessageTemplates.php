<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Customer group error message templates
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.09.2019
 * Time: 8:29
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup;

use OooAst\ImportExport\Model\Import\CustomerGroup\Validator\CanBeDeleted;
use OooAst\ImportExport\Model\Import\CustomerGroup\Validator\GroupID;
use OooAst\ImportExport\Model\Import\CustomerGroup\Validator\RequiredAttribute;
use OooAst\ImportExport\Model\Import\CustomerGroup\Validator\TaxClassExists;
use OooAst\ImportExport\Model\Import\RowValidatorInterface;

/**
 * Customer group import error message templates
 *
 * @package OooAst\ImportExport\Model\Import\CustomerGroup
 */
trait MessageTemplates
{
    /**
     * Message templates
     *
     * @var array
     */
    private $messageTemplates = [
        RowValidatorInterface::ERROR_ROW_IS_ORPHAN => 'Orphan rows that will be skipped due default row errors',
        CanBeDeleted::ERROR_GROUP_DOES_NOT_EXIST => 'Group with id "%d "does not exist.',
        CanBeDeleted::ERROR_GROUP_IS_USED => 'Group with id "%d" is used and therefore cannot be deleted.',
        RequiredAttribute::ERROR_ATTRIBUTE_REQUIRED => 'Attribute "%s" is required.',
        TaxClassExists::ERROR_CLASS_NOT_EXISTS => 'Tax class "%s" does not exist',
        GroupID::ERROR_ID_NOT_NUMBER => 'Group id "%d" is not a number.',
        GroupID::ERROR_ID_NOT_EXISTS => 'Group with id "%d" does not exist.',
    ];
}
