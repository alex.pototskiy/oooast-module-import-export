<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Check group id
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.09.2019
 * Time: 17:32
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup\Validator;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\ImportExport\Model\Import;
use OooAst\ImportExport\Model\Import\AbstractValidator;

/**
 * Check group if for update and add operations
 *
 * @package OooAst\ImportExport\Model\Import\CustomerGroup\Validator
 */
class GroupID extends AbstractValidator
{
    const HEADER_GROUP_ID = 'customer_group_id';
    const ERROR_ID_NOT_NUMBER = 'idNotNumber';
    const ERROR_ID_NOT_EXISTS = 'idNotExists';

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    public function __construct(
        GroupRepositoryInterface $groupRepository,
        array $validators = []
    ) {
        $this->groupRepository = $groupRepository;
        parent::__construct($validators);
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    public function isValid($value): bool
    {
        if ($this->context->getBehavior() != Import::BEHAVIOR_ADD_UPDATE) {
            return true;
        }
        $id = $value[self::HEADER_GROUP_ID];
        if (!is_numeric($id)) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->getMessageTemplate(self::ERROR_ID_NOT_NUMBER),
                        $id
                    )
                ]
            );
            return false;
        }
        $id = (int)$id;
        if ($id != -1) {
            try {
                $this->groupRepository->getById($id);
                return true;
            } catch (NoSuchEntityException $e) {
                $this->_addMessages([
                    sprintf(
                        $this->context->getMessageTemplate(self::ERROR_ID_NOT_EXISTS),
                        $id
                    )
                ]);
                return false;
            }
        }

        return true;
    }
}
