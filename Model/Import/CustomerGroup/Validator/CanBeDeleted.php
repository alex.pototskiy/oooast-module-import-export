<?php
/**
 * PHP version 7.1
 * Test that customer group can be deleted
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 11:49
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup\Validator;


use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\ImportExport\Model\Import;
use OooAst\ImportExport\Model\Import\AbstractValidator;
use OooAst\ImportExport\Model\Import\CustomerGroup;

/**
 * Class CanBeDeleted
 *
 * @package OooAst\ImportExport\Model\Import\CustomerGroup
 */
class CanBeDeleted extends AbstractValidator
{
    const ERROR_GROUP_DOES_NOT_EXIST = 'groupDoesNotExist';
    const ERROR_GROUP_IS_USED = 'groupIsUsed';
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $criteriaBuilder;

    /**
     * CanBeDeleted constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $criteriaBuilder
     * @param array $validators
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $criteriaBuilder,
        array $validators = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->criteriaBuilder = $criteriaBuilder;
        parent::__construct($validators);
    }

    /**
     * Test row data for delete possibility
     *
     * @param mixed $value
     *
     * @return bool
     * @throws LocalizedException
     */
    public function isValid($value): bool
    {
        if ($this->context->getBehavior($value) != Import::BEHAVIOR_DELETE) {
            return true;
        }
        /** @var CustomerGroup $customerGroup */
        $customerGroup = $this->context;
        $groupId = (int)$value[$customerGroup::COLUMN_ID];
        if (!$customerGroup->doesGroupExist($groupId)) {
            $this->_addMessages(
                [
                    sprintf(
                        $customerGroup->getMessageTemplate(self::ERROR_GROUP_DOES_NOT_EXIST),
                        $groupId
                    )
                ]
            );
            return false;
        }
        if ($this->isGroupUsed($groupId)) {
            $this->_addMessages(
                [
                    sprintf(
                        $customerGroup->getMessageTemplate(self::ERROR_GROUP_IS_USED),
                        $groupId
                    )
                ]
            );
            return false;
        }
        return true;
    }

    /**
     * Test if there is an user in this group
     *
     * @param int $groupId
     *
     * @return bool
     * @throws LocalizedException
     */
    private function isGroupUsed(int $groupId)
    {
        $criteria = $this->criteriaBuilder
            ->addFilter('group_id', (int)$groupId, 'eq')
            ->create();
        return $this->customerRepository->getList($criteria)->getTotalCount() > 0;
    }

}
