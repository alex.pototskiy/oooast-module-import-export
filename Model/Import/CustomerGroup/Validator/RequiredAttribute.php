<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Check required attributes
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.09.2019
 * Time: 8:41
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup\Validator;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use OooAst\ImportExport\Model\ColumnProvider;
use OooAst\ImportExport\Model\CustomerGroup\AttributeCollection;
use OooAst\ImportExport\Model\Import\AbstractValidator;

/**
 * Class RequiredAttribute
 *
 * @package OooAst\ImportExport\Model\Import\CustomerGroup\Validator
 */
class RequiredAttribute extends AbstractValidator
{
    const ERROR_ATTRIBUTE_REQUIRED = 'attributeRequired';
    /**
     * @var ColumnProvider
     */
    private $columnProvider;
    /**
     * @var AttributeCollection
     */
    private $attributeCollection;

    /**
     * RequiredAttribute constructor.
     *
     * @param ColumnProvider $columnProvider
     * @param AttributeCollection $attributeCollection
     * @param array $validators
     */
    public function __construct(
        ColumnProvider $columnProvider,
        AttributeCollection $attributeCollection,
        array $validators = []
    ) {
        $this->columnProvider = $columnProvider;
        $this->attributeCollection = $attributeCollection;
        parent::__construct($validators);
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     * @throws Exception
     */
    public function isValid($value): bool
    {
        $headers = $this->columnProvider->getHeaders($this->attributeCollection->get(), []);
        foreach ($headers as $header) {
            if (!isset($value[$header]) || empty($value[$header])) {
                $this->setInvalidAttribute($header);
                $this->_addMessages(
                    [
                        sprintf(
                            $this->context->getMessageTemplate(self::ERROR_ATTRIBUTE_REQUIRED),
                            $header
                        )
                    ]
                );
                return false;
            }
        }
        return true;
    }

}
