<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Test that tax class exists
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 22.09.2019
 * Time: 16:45
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup\Validator;

use OooAst\ImportExport\Model\CustomerGroup\Source\SourceTaxClass;
use OooAst\ImportExport\Model\Import\AbstractValidator;

/**
 * Class TaxClassExists
 *
 * @package OooAst\ImportExport\Model\Import\CustomerGroup\Validator
 */
class TaxClassExists extends AbstractValidator
{
    const HEADER_TAX_CLASS = 'tax_class';
    const ERROR_CLASS_NOT_EXISTS = 'classNotExists';

    /**
     * @var SourceTaxClass
     */
    private $sourceTaxClass;

    /**
     * TaxClassExists constructor.
     *
     * @param SourceTaxClass $sourceTaxClass
     * @param array $validators
     */
    public function __construct(
        SourceTaxClass $sourceTaxClass,
        array $validators = []
    ) {
        $this->sourceTaxClass = $sourceTaxClass;
        parent::__construct($validators);
    }

    /**
     * @inheritDoc
     */
    public function isValid($value): bool
    {
        $className = $value[self::HEADER_TAX_CLASS];
        $taxClass = array_filter(
            $this->sourceTaxClass->toOptionArray(),
            function ($value) use ($className) {
                return $value['label'] == $className;
            }
        );
        if (empty($taxClass)) {
            $this->_addMessages(
                [
                    sprintf(
                        $this->context->getMessageTemplate(self::ERROR_CLASS_NOT_EXISTS),
                        $className
                    )
                ]
            );
            return false;
        }
        return true;
    }
}
