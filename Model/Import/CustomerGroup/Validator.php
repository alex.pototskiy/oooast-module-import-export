<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Customer group import validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 11:09
 */

namespace OooAst\ImportExport\Model\Import\CustomerGroup;


use OooAst\ImportExport\Model\Import\AbstractValidator;

class Validator extends AbstractValidator
{
    public function __construct(array $validators = [])
    {
        parent::__construct($validators);
    }
}
