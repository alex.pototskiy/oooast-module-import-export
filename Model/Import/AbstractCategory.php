<?php
/**
 * PHP version 7.1
 * Abstract category import model
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 24.04.2019
 * Time: 11:15
 */

namespace OooAst\ImportExport\Model\Import;

use Magento\Catalog\Api\Data\CategoryAttributeInterface;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Export\Factory;
use Magento\ImportExport\Model\Import\Entity\AbstractEav;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ImportFactory;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;
use OooAst\ImportExport\Model\Traits\Category\ConstantsInterface;

/**
 * Class AbstractCategory
 *
 * @package OooAst\ImportExport\Model\Import
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
abstract class AbstractCategory extends AbstractEav implements ConstantsInterface
{
    const ERROR_CAN_NOT_SAVE_ENTITY = 'canNotSaveCategory';
    const ERROR_CAN_NOT_MOVE = 'canNotMove';
    const ERROR_PARENT_NOT_FOUND = 'parentNotFound';
    const ERROR_CAN_NOT_DELETE = 'canNotDelete';

    protected $messageTemplates = [
        RowValidatorInterface::ERROR_INVALID_SCOPE => 'Invalid value in Scope column',
        RowValidatorInterface::ERROR_INVALID_WEBSITE => 'Invalid website(%s)',
        RowValidatorInterface::ERROR_INVALID_CATEGORY => 'Category does not exist',
        RowValidatorInterface::ERROR_TRY_DELETE_TWICE => 'Category(skg:%s) is already deleted in import',
        RowValidatorInterface::ERROR_VALUE_IS_REQUIRED => 'Please make sure attribute(%s) is not empty',
        RowValidatorInterface::ERROR_SKG_IS_EMPTY => 'SKG is empty',
        RowValidatorInterface::ERROR_NO_DEFAULT_ROW => 'Default values row does not exist',
        RowValidatorInterface::ERROR_DUPLICATE_SCOPE => 'Duplicate scope',
        RowValidatorInterface::ERROR_DUPLICATE_SKG => 'Duplicate SKG',
        RowValidatorInterface::ERROR_ROW_IS_ORPHAN => 'Orphan rows that will be skipped due default row errors',
        RowValidatorInterface::ERROR_SKG_NOT_FOUND_FOR_DELETE => 'Category with specified SKG not found',
        RowValidatorInterface::ERROR_MEDIA_DATA_INCOMPLETE => 'Media data is incomplete',
        RowValidatorInterface::ERROR_INVALID_ATTRIBUTE_TYPE => 'Value for attribute(%s) contains incorrect value',
        RowValidatorInterface::ERROR_ABSENT_REQUIRED_ATTRIBUTE => 'Attribute(%s) is required',
        RowValidatorInterface::ERROR_INVALID_ATTRIBUTE_OPTION =>
            'Value for attribute(%s) is incorrect, see acceptable values on settings specified for Admin',
        RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE => 'Duplicated unique attribute',
        RowValidatorInterface::ERROR_INVALID_VARIATIONS_CUSTOM_OPTIONS =>
            'Value for \'%s\' sub attribute in \'%s\' attribute contains incorrect value, acceptable values are: '
            . '\'dropdown\', \'checkbox\', \'radio\', \'text\'',
        RowValidatorInterface::ERROR_INVALID_MEDIA_URL_OR_PATH => 'Wrong URL/path used for attribute(%s)',
        RowValidatorInterface::ERROR_MEDIA_URL_NOT_ACCESSIBLE =>
            'Imported resource (image) does not exist in the local media storage',
        RowValidatorInterface::ERROR_MEDIA_PATH_NOT_ACCESSIBLE =>
            'Imported resource (image) could not be downloaded from external resource due to '
            . 'timeout or access permissions',
        RowValidatorInterface::ERROR_DUPLICATE_URL_KEY =>
            'Url key: \'%s\' was already generated for an item with the SKU: \'%s\'. You need to '
            . 'specify the unique URL key manually',
        RowValidatorInterface::ERROR_DUPLICATE_MULTISELECT_VALUES => 'Value for multiselect attribute %s '
            . 'contains duplicated values',
        RowValidatorInterface::ERROR_NO_STORE_ROOT_CATEGORY => "Root category of store(%s) is not configured",
        RowValidatorInterface::ERROR_CAN_NOT_SAVE_ENTITY => 'Category can not be saved, error(%s).',
        RowValidatorInterface::ERROR_CAN_NOT_MOVE => "Category can not be move, error(%s)",
        RowValidatorInterface::ERROR_CAN_NOT_DELETE => "Category can not delete, error(%s)",
        RowValidatorInterface::ERROR_EXCEEDED_MAX_LENGTH => 'Attribute %s exceeded max length',
        RowValidatorInterface::ERROR_CATEGORY_ADDED_AND_DELETED => 'Category is added and deleted in one import',
        RowValidatorInterface::ERROR_CATEGORY_DELETED_AND_ADDED => 'Category is deleted and added in one import',
        RowValidatorInterface::ERROR_WRONG_PARENT => 'Wrong(does not exist) parent category',
        self::ERROR_CAN_NOT_SAVE_ENTITY => 'Can not save category from row %s',
        self::ERROR_CAN_NOT_MOVE => 'Can not move category from row %s',
        self::ERROR_PARENT_NOT_FOUND => 'Can not find parent for category in row %s',
        self::ERROR_CAN_NOT_DELETE => 'Can not delete category in row %s',
    ];

    /**
     * Permanent entity columns.
     *
     * @var string[]
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore
    protected $_specialAttributes = [
        self::COL_STORE,
        self::COL_WEBSITE,
        self::COLUMN_ACTION,
        self::COL_PARENT_SKG
    ];
    /**
     * List of attributes that are required but null is default value
     *
     * @var array
     */
    protected $nullValuesAttributes = [
        'available_sort_by',
        'default_sort_by'
    ];
    protected $masterAttributeCode = self::COL_SKG;

    /**
     * Category repository for import
     *
     * @var CategoryRepository
     */
    protected $categoryRepository;
    /**
     * Category entity type code
     *
     * @var string
     */
    private $entityTypeCode;

    /**
     * AbstractCategory constructor.
     *
     * @param StringUtils $string
     * @param ScopeConfigInterface $scopeConfig
     * @param ImportFactory $importFactory
     * @param Helper $resourceHelper
     * @param ResourceConnection $resource
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param StoreManagerInterface $storeManager
     * @param Factory $collectionFactory
     * @param Config $eavConfig
     * @param CategoryRepository $categoryRepository
     * @param array $data
     *
     * @throws LocalizedException
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        StringUtils $string,
        ScopeConfigInterface $scopeConfig,
        ImportFactory $importFactory,
        Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        StoreManagerInterface $storeManager,
        Factory $collectionFactory,
        Config $eavConfig,
        CategoryRepository $categoryRepository,
        array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->entityTypeCode = CategoryAttributeInterface::ENTITY_TYPE_CODE;
        parent::__construct(
            $string,
            $scopeConfig,
            $importFactory,
            $resourceHelper,
            $resource,
            $errorAggregator,
            $storeManager,
            $collectionFactory,
            $eavConfig,
            $data
        );
        $this->initErrorTemplates();
        $this->_initWebsites()->_initStores();
    }

    /**
     * Initialize Product error templates
     */
    protected function initErrorTemplates()
    {
        foreach ($this->messageTemplates as $errorCode => $template) {
            $this->addMessageTemplate($errorCode, $template);
        }
    }

    /**
     * Get list of required attributes with allowable null
     *
     * @return array
     */
    public function getNullValuesAttributes(): array
    {
        return $this->nullValuesAttributes;
    }

    /**
     * Imported entity type code getter
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return $this->entityTypeCode;
    }

    /**
     * Get error message template
     *
     * @param string $errorCode
     *
     * @return string|null
     */
    public function retrieveMessageTemplate(string $errorCode): ?string
    {
        if (isset($this->messageTemplates[$errorCode])) {
            return $this->messageTemplates[$errorCode];
        }
        return null;
    }
}
