<?php
/**
 * PHP version 7.1
 *
 * Category import behavior
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 23.04.2019
 * Time: 15:19
 */

namespace OooAst\ImportExport\Model\Import\Behavior;

use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Source\Import\AbstractBehavior;

/**
 * Category import possible behavior
 *
 * @package OooAst\ImportExport\Model\Import\Behavior
 */
class Basic extends AbstractBehavior
{

    /**
     * Get array of possible values
     *
     * @return array
     */
    public function toArray()
    {
        return [
            Import::BEHAVIOR_ADD_UPDATE => __("Add/Update"),
            Import::BEHAVIOR_DELETE => __("Delete")
        ];
    }

    /**
     * Get current behaviour group code
     *
     * @return string
     */
    public function getCode()
    {
        return 'category_import';
    }
}
