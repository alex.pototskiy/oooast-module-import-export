<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Row validator interface
 *
 * @category ImportExport
 * @package  OooAst_InportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 11:03
 */

namespace OooAst\ImportExport\Model\Import;

use Magento\Framework\Validator\ValidatorInterface;

/**
 * Interface RowValidatorInterface
 *
 * @package OooAst\ImportExport\Model\Import
 */
interface RowValidatorInterface extends ValidatorInterface
{
    const ERROR_ROW_IS_ORPHAN = 'rowIsOrphan';

    /**
     * Init validator with context import entity
     *
     * @param AbstractEntity $context
     * @param AbstractValidator|null $parentValidator
     *
     * @return mixed
     */
    public function init(AbstractEntity $context, ?AbstractValidator $parentValidator);
}
