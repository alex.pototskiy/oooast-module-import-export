<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Extended product import
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 12.08.2019
 * Time: 11:35
 */

namespace OooAst\ImportExport\Model\Import;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Config as CatalogConfig;
use Magento\Catalog\Model\Product\Url;
use Magento\Catalog\Model\ResourceModel\Product\LinkFactory;
use Magento\CatalogImportExport\Model\Import\Product as MageProduct;
use Magento\CatalogImportExport\Model\Import\Product\ImageTypeProcessor;
use Magento\CatalogImportExport\Model\Import\Product\OptionFactory;
use Magento\CatalogImportExport\Model\Import\Product\SkuProcessor;
use Magento\CatalogImportExport\Model\Import\Product\StoreResolver;
use Magento\CatalogImportExport\Model\Import\Product\TaxClassProcessor;
use Magento\CatalogImportExport\Model\Import\Product\Type\Factory;
use Magento\CatalogImportExport\Model\Import\Product\Validator;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\CatalogImportExport\Model\Import\Proxy\ProductFactory;
use Magento\CatalogImportExport\Model\Import\UploaderFactory;
use Magento\CatalogImportExport\Model\StockItemImporterInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory;
use Magento\CatalogInventory\Model\Spi\StockStateProviderInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Model\ResourceModel\Db\ObjectRelationProcessor;
use Magento\Framework\Model\ResourceModel\Db\TransactionManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\Store\Model\Store;
use OooAst\ImportExport\Helper\ArrayCsv;
use OooAst\ImportExport\Model\Helper\ImageName;
use Psr\Log\LoggerInterface;

/**
 * Class Product import entity
 *
 * @package OooAst\ImportExport\Model\Import
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Product extends MageProduct
{
    /**
     * @var array
     */
    private $bunchExistingImages;
    /**
     * @var string
     */
    private $currentRowSku;
    /**
     * @var int
     */
    private $currentRowStoreId;
    /**
     * @var MediaGalleryProcessor|Product\MediaGalleryProcessor|null
     */
    private $mediaProcessorExt;
    /**
     * @var ImageName
     */
    private $imageNameUtil;
    private $collectedRowImages;

    /**
     * Product constructor.
     *
     * @param ImageName $imageNameUtil
     * @param Data $jsonHelper
     * @param \Magento\ImportExport\Helper\Data $importExportData
     * @param \Magento\ImportExport\Model\ResourceModel\Import\Data $importData
     * @param Config $config
     * @param ResourceConnection $resource
     * @param Helper $resourceHelper
     * @param StringUtils $string
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param ManagerInterface $eventManager
     * @param StockRegistryInterface $stockRegistry
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockStateProviderInterface $stockStateProvider
     * @param \Magento\Catalog\Helper\Data $catalogData
     * @param \Magento\ImportExport\Model\Import\Config $importConfig
     * @param ResourceModelFactory $resourceFactory
     * @param OptionFactory $optionFactory
     * @param CollectionFactory $setColFactory
     * @param Factory $productTypeFactory
     * @param LinkFactory $linkFactory
     * @param ProductFactory $proxyProdFactory
     * @param UploaderFactory $uploaderFactory
     * @param Filesystem $filesystem
     * @param ItemFactory $stockResItemFac
     * @param TimezoneInterface $localeDate
     * @param DateTime $dateTime
     * @param LoggerInterface $logger
     * @param IndexerRegistry $indexerRegistry
     * @param StoreResolver $storeResolver
     * @param SkuProcessor $skuProcessor
     * @param Product\CategoryProcessor $categoryProcessor
     * @param Validator $validator
     * @param ObjectRelationProcessor $objectRelationProcessor
     * @param TransactionManagerInterface $transactionManager
     * @param TaxClassProcessor $taxClassProcessor
     * @param ScopeConfigInterface $scopeConfig
     * @param Url $productUrl
     * @param Product\MediaGalleryProcessor $mediaProcessor
     * @param array $data
     * @param array $dateAttrCodes
     * @param CatalogConfig|null $catalogConfig
     * @param ImageTypeProcessor|null $imageTypeProcessor
     * @param StockItemImporterInterface|null $stockItemImporter
     * @param DateTimeFactory|null $dateTimeFactory
     * @param ProductRepositoryInterface|null $productRepository
     *
     * @throws FileSystemException
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ImageName $imageNameUtil,
        Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        Config $config,
        ResourceConnection $resource,
        Helper $resourceHelper,
        StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        ManagerInterface $eventManager,
        StockRegistryInterface $stockRegistry,
        StockConfigurationInterface $stockConfiguration,
        StockStateProviderInterface $stockStateProvider,
        \Magento\Catalog\Helper\Data $catalogData,
        \Magento\ImportExport\Model\Import\Config $importConfig,
        ResourceModelFactory $resourceFactory,
        OptionFactory $optionFactory,
        CollectionFactory $setColFactory,
        Factory $productTypeFactory,
        LinkFactory $linkFactory,
        ProductFactory $proxyProdFactory,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        ItemFactory $stockResItemFac,
        TimezoneInterface $localeDate,
        DateTime $dateTime,
        LoggerInterface $logger,
        IndexerRegistry $indexerRegistry,
        StoreResolver $storeResolver,
        SkuProcessor $skuProcessor,
        Product\CategoryProcessor $categoryProcessor,
        Validator $validator,
        ObjectRelationProcessor $objectRelationProcessor,
        TransactionManagerInterface $transactionManager,
        TaxClassProcessor $taxClassProcessor,
        ScopeConfigInterface $scopeConfig,
        Url $productUrl,
        Product\MediaGalleryProcessor $mediaProcessor,
        array $data = [],
        array $dateAttrCodes = [],
        CatalogConfig $catalogConfig = null,
        ImageTypeProcessor $imageTypeProcessor = null,
        StockItemImporterInterface $stockItemImporter = null,
        DateTimeFactory $dateTimeFactory = null,
        ProductRepositoryInterface $productRepository = null
    ) {
        MageProduct::__construct(
            $jsonHelper,
            $importExportData,
            $importData,
            $config,
            $resource,
            $resourceHelper,
            $string,
            $errorAggregator,
            $eventManager,
            $stockRegistry,
            $stockConfiguration,
            $stockStateProvider,
            $catalogData,
            $importConfig,
            $resourceFactory,
            $optionFactory,
            $setColFactory,
            $productTypeFactory,
            $linkFactory,
            $proxyProdFactory,
            $uploaderFactory,
            $filesystem,
            $stockResItemFac,
            $localeDate,
            $dateTime,
            $logger,
            $indexerRegistry,
            $storeResolver,
            $skuProcessor,
            $categoryProcessor,
            $validator,
            $objectRelationProcessor,
            $transactionManager,
            $taxClassProcessor,
            $scopeConfig,
            $productUrl,
            $data,
            $dateAttrCodes,
            $catalogConfig,
            $imageTypeProcessor,
            $mediaProcessor,
            $stockItemImporter,
            $dateTimeFactory,
            $productRepository
        );
        $this->mediaProcessorExt = $mediaProcessor;
        $this->imageNameUtil = $imageNameUtil;
    }

    /**
     * @inheritDoc
     */
    public function parseMultiselectValues($values, $delimiter = MageProduct::PSEUDO_MULTI_LINE_SEPARATOR)
    {
        if (!empty($values)) {
            return ArrayCsv::toArray($values);
        } else {
            return [];
        }
    }

    /**
     * @inheritDoc
     *
     * Save current row sku
     */
    public function getImagesFromRow(array $rowData)
    {
        $this->currentRowSku = $rowData[MageProduct::COL_SKU];
        $this->currentRowStoreId = !empty($rowData[self::COL_STORE])
            ? $this->getStoreIdByCode($rowData[self::COL_STORE])
            : Store::DEFAULT_STORE_ID;
        list($images, $labels) = parent::getImagesFromRow($rowData);
        if (!empty($images['_media_image'])) {
            foreach ($images['_media_image'] as $image) {
                $this->collectedRowImages[$this->currentRowStoreId][$this->currentRowSku][$image] = 1;
            }
        } else {
            $this->collectedRowImages[$this->currentRowStoreId][$this->currentRowSku] = [];
        }
        return [$images, $labels];
    }

    /**
     * @inheritDoc
     *
     * Save current bunch existing images
     */
    protected function getExistingImages($bunch)
    {
        $this->bunchExistingImages = $this->mediaProcessorExt->getExistingImagesExt(
            array_column($bunch, MageProduct::COL_SKU)
        );
        return parent::getExistingImages($bunch);
    }

    /**
     * @inheritDoc
     *
     * Additionally test if image is exists
     */
    protected function uploadMediaFiles($fileName, $renameFileOff = false)
    {
        $indexedName = $this->imageNameUtil->imageIndexedName($fileName);
        if (!(isset($this->bunchExistingImages[$this->currentRowStoreId]) &&
            isset($this->bunchExistingImages[$this->currentRowStoreId][$this->currentRowSku]) &&
            isset($this->bunchExistingImages[$this->currentRowStoreId][$this->currentRowSku][$indexedName]))) {
            return parent::uploadMediaFiles($fileName, $renameFileOff);
        } else {
            return $indexedName;
        }
    }

    protected function _saveMediaGallery(array $mediaGalleryData)
    {
        $this->mediaProcessorExt->removeProductImages($this->bunchExistingImages, $this->collectedRowImages);
        return parent::_saveMediaGallery($mediaGalleryData); // TODO: Change the autogenerated stub
    }
}
