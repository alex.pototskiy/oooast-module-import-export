<?php
/**
 * PHP version 7.1
 * Import context
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 11:06
 */

namespace OooAst\ImportExport\Model\Import;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;

/**
 * Abstract import entity
 *
 * @package OooAst\ImportExport\Model\Import
 */
abstract class AbstractEntity extends \Magento\ImportExport\Model\Import\AbstractEntity
{
    /**
     * Get error message template
     *
     * @param string $msgCode
     *
     * @return mixed
     */
    abstract public function getMessageTemplate(string $msgCode);

    /**
     * Add row as skipped
     *
     * @param int $rowNum
     * @param string $errorCode Error code or simply column name
     * @param string $errorLevel error level
     * @param string|null $colName optional column name
     *
     * @return $this
     */
    protected function skipRow(
        $rowNum,
        string $errorCode,
        string $errorLevel = ProcessingError::ERROR_LEVEL_NOT_CRITICAL,
        $colName = null
    ): self {
        $this->addRowError($errorCode, $rowNum, $colName, null, $errorLevel);
        $this->getErrorAggregator()->addRowToSkip($rowNum);
        return $this;
    }
}
