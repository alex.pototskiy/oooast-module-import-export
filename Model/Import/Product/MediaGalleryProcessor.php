<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Product import media processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 13.08.2019
 * Time: 20:34
 */

namespace OooAst\ImportExport\Model\Import\Product;

use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\CatalogImportExport\Model\Import\Product\MediaGalleryProcessor as MageMediaGalleryProcessor;
use Magento\CatalogImportExport\Model\Import\Product\SkuProcessor;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModel;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use OooAst\ImportExport\Model\Helper\ImageName;

class MediaGalleryProcessor extends MageMediaGalleryProcessor
{
    /**
     * @var ProcessingErrorAggregatorInterface
     */
    private $errorAggregator;
    /**
     * DB connection.
     *
     * @var AdapterInterface
     */
    private $connection;
    /**
     * @var ResourceModelFactory
     */
    private $resourceFactory;
    /**
     * @var ResourceModel
     */
    private $resourceModel;
    /**
     * @var string
     */
    private $productEntityTable;
    /**
     * @var string
     */
    private $mediaGalleryTable;
    /**
     * @var string
     */
    private $galleryValueTable;
    /**
     * @var string
     */
    private $galleryValueToEntityTable;
    /**
     * @var ImageName
     */
    private $imageNameUtil;

    /**
     * MediaGalleryProcessor constructor.
     *
     * @param SkuProcessor $skuProcessor
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     * @param ResourceModelFactory $resourceModelFactory
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param ImageName $imageNameUtil
     */
    public function __construct(
        SkuProcessor $skuProcessor,
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection,
        ResourceModelFactory $resourceModelFactory,
        ProcessingErrorAggregatorInterface $errorAggregator,
        ImageName $imageNameUtil
    ) {
        $this->imageNameUtil = $imageNameUtil;
        parent::__construct(
            $skuProcessor,
            $metadataPool,
            $resourceConnection,
            $resourceModelFactory,
            $errorAggregator
        );
        $this->connection = $resourceConnection->getConnection();
        $this->errorAggregator = $errorAggregator;
        $this->resourceFactory = $resourceModelFactory;
        $this->initMediaGalleryResources();
    }

    /**
     * Init media gallery resources.
     *
     * @return void
     */
    private function initMediaGalleryResources()
    {
        $this->productEntityTable = $this->getResource()->getTable('catalog_product_entity');
        $this->mediaGalleryTable = $this->getResource()->getTable(Gallery::GALLERY_TABLE);
        $this->galleryValueTable = $this->getResource()->getTable(Gallery::GALLERY_VALUE_TABLE);
        $this->galleryValueToEntityTable = $this->getResource()->getTable(
            Gallery::GALLERY_VALUE_TO_ENTITY_TABLE
        );
    }

    /**
     * Get resource.
     *
     * @return ResourceModel
     */
    private function getResource()
    {
        if (!$this->resourceModel) {
            $this->resourceModel = $this->resourceFactory->create();
        }
        return $this->resourceModel;
    }

    /**
     * Get existing product images
     *
     * @param array $skus
     *
     * @return array
     */
    public function getExistingImagesExt(array $skus): array
    {
        $result = [];
        if ($this->errorAggregator->hasToBeTerminated()) {
            return $result;
        }
        $stringFunc = function ($value) {
            return (string)$value;
        };
        $productSKUs = array_map($stringFunc, $skus);
        $select = $this->connection->select()
            ->from(
                ['mg' => $this->mediaGalleryTable],
                ['value' => 'mg.value']
            )
            ->joinInner(
                ['mgvte' => $this->galleryValueToEntityTable],
                '(mgvte.value_id = mg.value_id)',
                [
                    'value_id' => 'mgvte.value_id',
                    'entity_id' => 'mgvte.entity_id'
                ]
            )
            ->joinLeft(
                ['mgv' => $this->galleryValueTable],
                '(mgv.value_id = mg.value_id AND mgv.entity_id = mgvte.entity_id)',
                [
                    'store_id' => 'mgv.store_id',
                    'label' => 'mgv.label',
                    'position' => 'mgv.position',
                    'disabled' => 'mgv.disabled'
                ]
            )
            ->joinInner(
                ['pe' => $this->productEntityTable],
                '(pe.entity_id = mgvte.entity_id)',
                ['sku' => 'pe.sku', 'product_id' => 'pe.entity_id']
            )
            ->where('pe.sku in (?)', $productSKUs);
        foreach ($this->connection->fetchAll($select) as $image) {
            $result[$image['store_id']][$image['sku']][$image['value']] = $image;
        }
        return $result;
    }

    /**
     * Remove existing product images
     *
     * @param array $images Images in format:
     * 'store_id' => [
     *      'sku' => [
     *          'value' => ...,
     *          'value_id' => ...,
     *          'entity_id' => ...,
     *          'store_id' => ...,
     *          'label' => ...,
     *          'position' => ...,
     *          'disabled' => ...,
     *          'sku' => ...
     *      ]
     * ]
     * @param array $rowImages
     */
    public function removeProductImages(array $images, array $rowImages)
    {
        $valueIDs = [];
        foreach ($images as $storeImages) {
            foreach ($storeImages as $skuImages) {
                foreach ($skuImages as $image) {
                    if (isset($rowImages[$image['store_id']]) && isset($rowImages[$image['store_id']][$image['sku']])) {
                        $importedImages = array_flip(
                            array_map(
                                function ($v) {
                                    return $this->imageNameUtil->imageIndexedName($v);
                                },
                                array_keys($rowImages[$image['store_id']][$image['sku']])
                            )
                        );
                        if (!isset($importedImages[$image['value']])) {
                            $valueIDs[$image['value_id']]['store_id'] = $image['store_id'];
                            $valueIDs[$image['value_id']]['product_id'] = $image['product_id'];
                        }
                    } else {
                        $valueIDs[$image['value_id']]['store_id'] = $image['store_id'];
                        $valueIDs[$image['value_id']]['product_id'] = $image['product_id'];
                    }
                }
            }
        }
        foreach ($valueIDs as $valueID => $product) {
            $this->connection->delete(
                $this->galleryValueToEntityTable,
                "value_id = {$valueID} and entity_id = {$product['product_id']}"
            );
            $this->connection->delete(
                $this->galleryValueTable,
                "value_id = {$valueID} and entity_id = {$product['product_id']} and store_id = {$product['store_id']}"
            );
        }
    }
}
