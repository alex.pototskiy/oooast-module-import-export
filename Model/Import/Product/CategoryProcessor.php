<?php
/**
 * PHP version 7.1
 * Product import category processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 13.08.2019
 * Time: 17:30
 */

namespace OooAst\ImportExport\Model\Import\Product;


use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor as MageCategoryProcessor;
use Magento\Framework\Exception\AlreadyExistsException;
use OooAst\ImportExport\Helper\ArrayCsv;

/**
 * Class CategoryProcessor
 *
 * @package OooAst\ImportExport\Model\Import\Product
 */
class CategoryProcessor extends MageCategoryProcessor
{
    /**
     * CategoryProcessor constructor.
     *
     * @param CollectionFactory $categoryColFactory
     * @param CategoryFactory $categoryFactory
     *
     * phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod.Found
     */
    public function __construct(
        CollectionFactory $categoryColFactory,
        CategoryFactory $categoryFactory
    ) {
        // phpcs:enable
        parent::__construct($categoryColFactory, $categoryFactory);
    }

    /**
     * Convert category names to ids
     *
     * @param string $categoriesString
     * @param string $categoriesSeparator
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function upsertCategories($categoriesString, $categoriesSeparator)
    {
        $categoriesIds = [];
        $categories = ArrayCsv::toArray($categoriesString);

        foreach ($categories as $category) {
            $categoriesIds[] = $this->upsertCategory($category);
        }

        return $categoriesIds;
    }

    /**
     * Add failed category
     *
     * @param string $category
     * @param AlreadyExistsException $exception
     *
     * @return $this
     */
    private function addFailedCategory($category, $exception)
    {
        $this->failedCategories[] =
            [
                'category' => $category,
                'exception' => $exception,
            ];
        return $this;
    }
}
