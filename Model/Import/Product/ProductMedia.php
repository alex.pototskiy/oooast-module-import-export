<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Product media import
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 27.07.2019
 * Time: 8:08
 */

namespace OooAst\ImportExport\Model\Import\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Config as CatalogConfig;
use Magento\Catalog\Model\Product\Url;
use Magento\Catalog\Model\ResourceModel\Product\LinkFactory;
use Magento\CatalogImportExport\Model\Import\Product;
use Magento\CatalogImportExport\Model\Import\Product\ImageTypeProcessor;
use Magento\CatalogImportExport\Model\Import\Product\MediaGalleryProcessor;
use Magento\CatalogImportExport\Model\Import\Product\OptionFactory;
use Magento\CatalogImportExport\Model\Import\Product\Type\Factory;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\CatalogImportExport\Model\Import\Proxy\ProductFactory;
use Magento\CatalogImportExport\Model\Import\UploaderFactory;
use Magento\CatalogImportExport\Model\StockItemImporterInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory;
use Magento\CatalogInventory\Model\Spi\StockStateProviderInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Model\ResourceModel\Db\ObjectRelationProcessor;
use Magento\Framework\Model\ResourceModel\Db\TransactionManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Helper\Data;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Psr\Log\LoggerInterface;

class ProductMedia extends Product
{
    /**
     * Container for filesystem object.
     *
     * @var Filesystem
     */
    private $filesystem;
    /**
     * Media processor
     *
     * @var MediaGalleryProcessor
     */
    private $mediaProcessor;
    /**
     * @var ImageTypeProcessor
     */
    private $imageTypeProcessor;
    /**
     * @var array|null
     */
    private $fieldToAttrMap;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        Config $config,
        ResourceConnection $resource,
        Helper $resourceHelper,
        StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        ManagerInterface $eventManager,
        StockRegistryInterface $stockRegistry,
        StockConfigurationInterface $stockConfiguration,
        StockStateProviderInterface $stockStateProvider,
        \Magento\Catalog\Helper\Data $catalogData,
        \Magento\ImportExport\Model\Import\Config $importConfig,
        ResourceModelFactory $resourceFactory,
        OptionFactory $optionFactory,
        CollectionFactory $setColFactory,
        Factory $productTypeFactory,
        LinkFactory $linkFactory,
        ProductFactory $proxyProdFactory,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        ItemFactory $stockResItemFac,
        TimezoneInterface $localeDate,
        DateTime $dateTime,
        LoggerInterface $logger,
        IndexerRegistry $indexerRegistry,
        Product\StoreResolver $storeResolver,
        Product\SkuProcessor $skuProcessor,
        Product\CategoryProcessor $categoryProcessor,
        Product\Validator $validator,
        ObjectRelationProcessor $objectRelationProcessor,
        TransactionManagerInterface $transactionManager,
        Product\TaxClassProcessor $taxClassProcessor,
        ScopeConfigInterface $scopeConfig,
        Url $productUrl,
        array $data = [],
        array $dateAttrCodes = [],
        CatalogConfig $catalogConfig = null,
        ImageTypeProcessor $imageTypeProcessor = null,
        MediaGalleryProcessor $mediaProcessor = null,
        StockItemImporterInterface $stockItemImporter = null,
        DateTimeFactory $dateTimeFactory = null,
        ProductRepositoryInterface $productRepository = null
    ) {
        parent::__construct(
            $jsonHelper, $importExportData, $importData, $config, $resource, $resourceHelper, $string, $errorAggregator,
            $eventManager, $stockRegistry, $stockConfiguration, $stockStateProvider, $catalogData, $importConfig,
            $resourceFactory, $optionFactory, $setColFactory, $productTypeFactory, $linkFactory, $proxyProdFactory,
            $uploaderFactory, $filesystem, $stockResItemFac, $localeDate, $dateTime, $logger, $indexerRegistry,
            $storeResolver, $skuProcessor, $categoryProcessor, $validator, $objectRelationProcessor,
            $transactionManager, $taxClassProcessor, $scopeConfig, $productUrl, $data, $dateAttrCodes, $catalogConfig,
            $imageTypeProcessor, $mediaProcessor, $stockItemImporter, $dateTimeFactory, $productRepository
        );
        $this->filesystem = $filesystem;
        $this->mediaProcessor = $mediaProcessor;
        $this->imageTypeProcessor = $imageTypeProcessor;
        $this->fieldToAttrMap = array_flip($this->_fieldsMap);
    }

    /**
     * Get store resolver
     *
     * @return Product\StoreResolver
     */
    public function getStoreResolver(): Product\StoreResolver
    {
        return $this->storeResolver;
    }

    /**
     * Get product type models
     *
     * @return array
     */
    public function getProductTypeModels(): array
    {
        return $this->_productTypeModels;
    }

    /**
     * Get production factory
     *
     * @return ProductFactory
     */
    public function getProxyProdFactory(): ProductFactory
    {
        return $this->_proxyProdFactory;
    }

    /**
     * Get existing product images
     *
     * @param array $bunch
     *
     * @return array
     */
    public function getExistingImagesWrapper($bunch)
    {
        return parent::getExistingImages($bunch); // TODO: Change the autogenerated stub
    }

    /**
     * Upload image files
     *
     * @param $fileName
     * @param bool $renameFileOff
     *
     * @return string
     */
    public function uploadMediaFilesWrapper($fileName, $renameFileOff = false)
    {
        return parent::uploadMediaFiles($fileName, $renameFileOff); // TODO: Change the autogenerated stub
    }

    /**
     * Prepare array with image states (visible or hidden from product page)
     *
     * @param array $rowData
     *
     * @return array
     */
    public function getImagesHiddenStates($rowData)
    {
        $statesArray = [];
        $mappingArray = [
            '_media_is_disabled' => '1'
        ];

        foreach ($mappingArray as $key => $value) {
            if (isset($rowData[$key]) && strlen(trim($rowData[$key]))) {
                $items = explode($this->getMultipleValueSeparator(), $rowData[$key]);

                foreach ($items as $item) {
                    $statesArray[$item] = $value;
                }
            }
        }

        return $statesArray;
    }

    /**
     * Try to find file by it's path.
     *
     * @param string $fileName
     *
     * @return string
     */
    public function getSystemFile($fileName)
    {
        $filePath = 'catalog' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR . $fileName;
        /** @var ReadInterface $read */
        $read = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);

        return $read->isExist($filePath) && $read->isReadable($filePath) ? $fileName : '';
    }

    /**
     * Save product images
     *
     * @param array $mediaGalleryData
     *
     * @return Product
     */
    public function saveMediaGalleryWrapper(array $mediaGalleryData)
    {
        return parent::_saveMediaGallery($mediaGalleryData); // TODO: Change the autogenerated stub
    }

    /**
     * Update 'disabled' field for media gallery entity
     *
     * @param array $images
     *
     * @return $this
     */
    public function updateMediaGalleryVisibility(array $images)
    {
        if (!empty($images)) {
            $this->mediaProcessor->updateMediaGalleryVisibility($images);
        }

        return $this;
    }

    /**
     * Update media gallery labels
     *
     * @param array $labels
     *
     * @return void
     */
    public function updateMediaGalleryLabels(array $labels)
    {
        if (!empty($labels)) {
            $this->mediaProcessor->updateMediaGalleryLabels($labels);
        }
    }

    /**
     * Convert field name to attribute name
     *
     * @param string $field
     *
     * @return string
     */
    public function getMediaKey(string $field): string
    {
        return array_key_exists($field, $this->fieldToAttrMap) ? $this->fieldToAttrMap[$field] : $field;
    }

    /**
     * Check if product exists for specified SKU
     *
     * @param string $sku
     *
     * @return bool
     */
    public function isSkuExist($sku)
    {
        $sku = strtolower($sku);
        return isset($this->_oldSku[$sku]);
    }

    /**
     * Save product attributes
     *
     * @param array $attributesData
     *
     * @return Product
     */
    function saveProductAttributesWrapper(array $attributesData)
    {
        return parent::_saveProductAttributes($attributesData); // TODO: Change the autogenerated stub
    }
}
