<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Product import image processor
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 28.07.2019
 * Time: 9:41
 */

namespace OooAst\ImportExport\Model\Import\Product;

use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModel;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

/**
 * Class MediaProcessor
 *
 * @package OooAst\ImportExport\Model\Import\Product
 */
class MediaProcessor
{
    /**
     * @var AdapterInterface
     */
    private $connection;
    /**
     * @var MetadataPool
     */
    private $metadataPool;
    /**
     * @var ProcessingErrorAggregatorInterface
     */
    private $errorAggregator;
    /**
     * @var ResourceModelFactory
     */
    private $resourceModelFactory;
    /**
     * @var ResourceModel
     */
    private $resourceModel;
    /**
     * @var string
     */
    private $productEntityTableName;
    /**
     * @var string
     */
    private $mediaGalleryTableName;
    /**
     * @var string
     */
    private $mediaGalleryValueTableName;
    /**
     * @var string
     */
    private $mediaGalleryValueToEntityTableName;

    /**
     * MediaProcessor constructor.
     *
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     * @param ResourceModelFactory $resourceModelFactory
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     */
    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection,
        ResourceModelFactory $resourceModelFactory,
        ProcessingErrorAggregatorInterface $errorAggregator
    ) {
        $this->metadataPool = $metadataPool;
        $this->connection = $resourceConnection->getConnection();
        $this->errorAggregator = $errorAggregator;
        $this->resourceModelFactory = $resourceModelFactory;
        $this->initMediaResources();
    }

    /**
     * Init product media resources
     */
    private function initMediaResources()
    {
        $this->productEntityTableName = $this->getResource()->getTable('catalog_product_entity');
        $this->mediaGalleryTableName = $this->getResource()->getTable(Gallery::GALLERY_TABLE);
        $this->mediaGalleryValueTableName = $this->getResource()->getTable(Gallery::GALLERY_VALUE_TABLE);
        $this->mediaGalleryValueToEntityTableName = $this->getResource()->getTable(
            Gallery::GALLERY_VALUE_TO_ENTITY_TABLE
        );
    }

    /**
     * Get resource model
     *
     * @return ResourceModel
     */
    private function getResource(): ResourceModel
    {
        if (!$this->resourceModel) {
            $this->resourceModel = $this->resourceModelFactory->create();
        }
        return $this->resourceModel;
    }

    /**
     * Get existing product images
     *
     * @param array $skus
     *
     * @return array
     */
    public function getExistingImages(array $skus): array
    {
        $result = [];
        if ($this->errorAggregator->hasToBeTerminated()) {
            return $result;
        }
        $stringFunc = function ($value) {
            return (string)$value;
        };
        $productSKUs = array_map($stringFunc, $skus);
        $select = $this->connection->select()
            ->from(
                ['mg' => $this->mediaGalleryTableName],
                ['value' => 'mg.value']
            )
            ->joinInner(
                ['mgvte' => $this->mediaGalleryValueToEntityTableName],
                '(mgvte.value_id = mg.value_id)',
                [
                    'value_id' => 'mgvte.value_id',
                    'entity_id' => 'mgvte.entity_id'
                ]
            )
            ->joinLeft(
                ['mgv' => $this->mediaGalleryValueTableName],
                '(mgv.value_id = mg.value_id AND mgv.entity_id = mgvte.entity_id)',
                [
                    'store_id' => 'mgv.store_id',
                    'label' => 'mgv.label',
                    'position' => 'mgv.position',
                    'disabled' => 'mgv.disabled'
                ]
            )
            ->joinInner(
                ['pe' => $this->productEntityTableName],
                '(pe.entity_id = mgvte.entity_id)',
                ['sku' => 'pe.sku', 'product_id' => 'pe.entity_id']
            )
            ->where('pe.sku in (?)', $productSKUs);
        foreach ($this->connection->fetchAll($select) as $image) {
            $result[$image['store_id']][$image['sku']][$image['value']] = $image;
        }
        return $result;
    }

    /**
     * Remove existing product images
     *
     * @param array $images Images in format:
     * 'store_id' => [
     *      'sku' => [
     *          'value' => ...,
     *          'value_id' => ...,
     *          'entity_id' => ...,
     *          'store_id' => ...,
     *          'label' => ...,
     *          'position' => ...,
     *          'disabled' => ...,
     *          'sku' => ...
     *      ]
     * ]
     * @param array $rowImages
     */
    public function removeProductImages(array $images, array $rowImages)
    {
        $valueIDs = [];
        foreach ($images as $storeImages) {
            foreach ($storeImages as $skuImages) {
                foreach ($skuImages as $image) {
                    if (!(isset($rowImages[$image['store_id']]) &&
                        isset($rowImages[$image['store_id']][$image['sku']]) &&
                        isset($rowImages[$image['store_id']][$image['sku']][$image['value']]))) {
                        $valueIDs[$image['value_id']]['store_id'] = $image['store_id'];
                        $valueIDs[$image['value_id']]['product_id'] = $image['product_id'];
                    }
                }
            }
        }
        foreach ($valueIDs as $valueID => $product) {
            $this->connection->delete(
                $this->mediaGalleryValueToEntityTableName,
                "value_id = {$valueID} and entity_id = {$product['product_id']}"
            );
            $this->connection->delete(
                $this->mediaGalleryValueTableName,
                "value_id = {$valueID} and entity_id = {$product['product_id']} and store_id = {$product['store_id']}"
            );
        }
    }
}
