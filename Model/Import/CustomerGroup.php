<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Import Customer Group
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 8:04
 */

namespace OooAst\ImportExport\Model\Import;

use Magento\Customer\Api\Data\GroupInterfaceFactory;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ImportFactory;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\Tax\Api\Data\TaxClassInterface;
use Magento\Tax\Api\TaxClassRepositoryInterface;
use OooAst\ImportExport\Model\CustomerGroup\Constants;
use OooAst\ImportExport\Model\CustomerGroup\Source\SourceTaxClass;
use OooAst\ImportExport\Model\Import\CustomerGroup\MessageTemplates;
use OooAst\ImportExport\Model\Import\CustomerGroup\Validator;

/**
 * Class CustomerGroup
 *
 * @package OooAst\ImportExport\Model\Import
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomerGroup extends AbstractEntity implements Constants
{
    use \OooAst\ImportExport\Model\CustomerGroup\CustomerGroup;
    use MessageTemplates;

    /**
     * Tax class sources
     *
     * @var SourceTaxClass
     */
    private $taxClassSource;
    /**
     * Tax class repository
     *
     * @var TaxClassRepositoryInterface
     */
    private $taxClassRepository;
    /**
     * Tax class interface
     *
     * @var TaxClassInterface
     */
    private $taxClass;
    /**
     * @var SearchCriteriaBuilder
     */
    private $criteriaBuilder;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;
    /**
     * @var GroupInterfaceFactory
     */
    private $groupInterfaceFactory;

    /**
     * CustomerGroup constructor.
     *
     * @param StringUtils $string
     * @param ScopeConfigInterface $scopeConfig
     * @param ImportFactory $importFactory
     * @param Helper $resourceHelper
     * @param ResourceConnection $resource
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param SourceTaxClass $taxClassSource
     * @param TaxClassRepositoryInterface $taxClassRepository
     * @param TaxClassInterface $taxClass
     * @param SearchCriteriaBuilder $criteriaBuilder
     * @param GroupRepositoryInterface $groupRepository
     * @param GroupInterfaceFactory $groupInterfaceFactory
     * @param Validator $validator
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        StringUtils $string,
        ScopeConfigInterface $scopeConfig,
        ImportFactory $importFactory,
        Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        SourceTaxClass $taxClassSource,
        TaxClassRepositoryInterface $taxClassRepository,
        TaxClassInterface $taxClass,
        SearchCriteriaBuilder $criteriaBuilder,
        GroupRepositoryInterface $groupRepository,
        GroupInterfaceFactory $groupInterfaceFactory,
        Validator $validator,
        array $data = []
    ) {
        $this->groupInterfaceFactory = $groupInterfaceFactory;
        $this->groupRepository = $groupRepository;
        $this->taxClassSource = $taxClassSource;
        $this->taxClassRepository = $taxClassRepository;
        $this->taxClass = $taxClass;
        $this->masterAttributeCode = self::COLUMN_ID;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->validator = $validator;
        $this->validator->init($this, null);
        parent::__construct(
            $string,
            $scopeConfig,
            $importFactory,
            $resourceHelper,
            $resource,
            $errorAggregator,
            $data
        );
    }

    /**
     * Imported entity type code getter
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE_CODE;
    }

    /**
     * Validate data row
     *
     * @param array $rowData
     * @param int $rowNumber
     *
     * @return bool
     */
    public function validateRow(array $rowData, $rowNumber)
    {
        if ($this->validator->isValid($rowData)) {
            return true;
        } else {
            foreach ($this->validator->getMessages() as $message) {
                $this->skipRow(
                    $rowNumber,
                    $message,
                    ProcessingError::ERROR_LEVEL_NOT_CRITICAL,
                    $this->validator->getInvalidAttribute()
                );
            }
            return false;
        }
    }

    /**
     * Test if group exists
     *
     * @param int $groupId
     *
     * @return bool
     */
    public function doesGroupExist(int $groupId)
    {
        try {
            $this->groupRepository->getById($groupId);
            return true;
        } catch (NoSuchEntityException|LocalizedException $e) {
            return false;
        }
    }

    /**
     * Get error message template
     *
     * @param string $msgCode
     *
     * @return string
     */
    public function getMessageTemplate(string $msgCode): string
    {
        if (isset($this->messageTemplates[$msgCode])) {
            return $this->messageTemplates[$msgCode];
        } else {
            return "Error message with unknown code: $msgCode";
        }
    }

    /**
     * Import data rows
     *
     * @return boolean
     *
     * @throws InputException
     * @throws InvalidTransitionException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    protected function _importData()
    {
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                switch ($this->getBehavior($rowData)) {
                    case 'add_update':
                        $this->processAddUpdate($rowData);
                        break;
                    case 'delete':
                        $this->processDelete($rowData);
                        break;
                }
            }
        }
        return true;
    }

    /**
     * Create or update group
     *
     * @param $rowData
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws InvalidTransitionException
     */
    private function processAddUpdate($rowData)
    {
        $id = (int)$rowData[self::COLUMN_ID];
        $taxClass = current(
            array_filter(
                $this->taxClassSource->toOptionArray(),
                function ($class) use ($rowData) {
                    return $class['label'] == $rowData[self::HEADER_TAX_CLASS];
                }
            )
        );
        $group = $id == -1 ?
            $group = $this->groupInterfaceFactory->create() :
            $group = $this->groupRepository->getById($id);
        $group->setCode($rowData[self::COLUMN_CODE]);
        $group->setTaxClassId($taxClass['value']);
        $group->setTaxClassName($taxClass['label']);
        $this->groupRepository->save($group);
    }

    /**
     * Delete group
     *
     * @param $rowData
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws StateException
     */
    private function processDelete($rowData)
    {
        $id = (int)$rowData[self::COLUMN_ID];
        $this->groupRepository->deleteById($id);
    }
}
