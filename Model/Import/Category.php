<?php
/**
 * PHP version 7.1
 *
 * Category import model
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 13.04.2019
 * Time: 11:08
 */

declare(strict_types=1);

namespace OooAst\ImportExport\Model\Import;

use Exception;
use Magento\Catalog\Api\CategoryAttributeRepositoryInterface;
use Magento\Catalog\Model\Category\Attribute;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Export\Factory;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ImportFactory;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\Store\Model\StoreManagerInterface;
use OooAst\ImportExport\Model\Helper\CategoryRepository;
use OooAst\ImportExport\Model\Helper\SearchCriteriaHelper;
use OooAst\ImportExport\Model\Import\Category\Processor;
use OooAst\ImportExport\Model\Import\Category\RowValidatorInterface;
use OooAst\ImportExport\Model\Import\Category\SkgProcessor;
use OooAst\ImportExport\Model\Import\Category\StoreResolver;
use OooAst\ImportExport\Model\Import\Category\Type\CategoryType;
use Zend_Validate_Exception;

/**
 * Category import model
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 *
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Category extends AbstractCategory
{
    /**
     * @var CategoryAttributeRepositoryInterface
     */
    private $categoryAttributeRepository;
    /** @var CategoryType */
    private $categoryType;
    /** @var Category\Validator */
    private $validator;
    /**
     * @var SkgProcessor
     */
    private $skgProcessor;
    /** @var StoreResolver */
    private $storeResolver;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Processor
     */
    /**
     * Category processor
     *
     * @var Processor
     */
    private $processor;
    /**
     * Search criteria helper
     *
     * @var SearchCriteriaHelper
     */
    private $criteriaHelper;

    /**
     * Category import model constructor.
     *
     * @param StringUtils $string
     * @param ScopeConfigInterface $scopeConfig
     * @param ImportFactory $importFactory
     * @param Helper $resourceHelper
     * @param ResourceConnection $resource
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param StoreManagerInterface $storeManager
     * @param Factory $collectionFactory
     * @param Config $eavConfig
     * @param CategoryRepository $categoryRepository
     * @param CategoryAttributeRepositoryInterface $categoryAttrRepo
     * @param Category\Type\CategoryTypeFactory $categoryTypeFactory
     * @param SearchCriteriaHelper $criteriaHelper
     * @param Category\Validator $validator
     * @param SkgProcessor $skgProcessor
     * @param StoreResolver $storeResolver
     * @param Filesystem $filesystem
     * @param Processor $processor
     * @param array $data
     *
     * @throws LocalizedException
     * @throws Exception
     */
    public function __construct(
        StringUtils $string,
        ScopeConfigInterface $scopeConfig,
        ImportFactory $importFactory,
        Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        StoreManagerInterface $storeManager,
        Factory $collectionFactory,
        Config $eavConfig,
        CategoryRepository $categoryRepository,
        CategoryAttributeRepositoryInterface $categoryAttrRepo,
        Category\Type\CategoryTypeFactory $categoryTypeFactory,
        SearchCriteriaHelper $criteriaHelper,
        Category\Validator $validator,
        SkgProcessor $skgProcessor,
        StoreResolver $storeResolver,
        Filesystem $filesystem,
        Processor $processor,
        array $data = []
    ) {
        $this->criteriaHelper = $criteriaHelper;
        $this->processor = $processor;
        $this->filesystem = $filesystem;
        $this->storeResolver = $storeResolver;
        $this->categoryAttributeRepository = $categoryAttrRepo;
        $this->categoryType = $categoryTypeFactory->create(['entityModel' => $this]);
        $this->validator = $validator;
        $this->skgProcessor = $skgProcessor;

        parent::__construct(
            $string,
            $scopeConfig,
            $importFactory,
            $resourceHelper,
            $resource,
            $errorAggregator,
            $storeManager,
            $collectionFactory,
            $eavConfig,
            $categoryRepository,
            $data
        );
        /** @var Attribute $attribute */
        foreach ($categoryAttrRepo->getList($this->criteriaHelper->createEmptyCriteria())->getItems() as $attribute) {
            $this->_attributeCollection->addItem($attribute);
        }
        $this->validator->init($this);
        $this->processor->init($this);
    }

    /**
     * Parse values of multiselect attributes depends on "Fields Enclosure" parameter
     *
     * @param string $values
     * @param string $delimiter
     *
     * @return array
     */
    public function parseMultiselectValues(
        string $values,
        string $delimiter = self::PSEUDO_MULTI_LINE_SEPARATOR
    ): array {
        if (empty($this->_parameters[Import::FIELDS_ENCLOSURE])) {
            return explode($delimiter, $values);
        }
        if (preg_match_all('~"((?:[^"]|"")*)"~', $values, $matches)) {
            return $values = array_map(
                function ($val) {
                    return str_replace('""', '"', $val);
                },
                $matches[1]
            );
        }
        return [$values];
    }

    /**
     * Return empty attribute value constant
     *
     * @return string
     * @since 101.0.0
     */
    public function getEmptyAttributeValueConstant()
    {
        if (!empty($this->_parameters[Import::FIELD_EMPTY_ATTRIBUTE_VALUE_CONSTANT])) {
            return $this->_parameters[Import::FIELD_EMPTY_ATTRIBUTE_VALUE_CONSTANT];
        }
        return Import::DEFAULT_EMPTY_ATTRIBUTE_VALUE_CONSTANT;
    }

    /**
     * Multiple value separator
     *
     * @return string
     */
    public function getMultipleValueSeparator()
    {
        if (!empty($this->_parameters[Import::FIELD_FIELD_MULTIPLE_VALUE_SEPARATOR])) {
            return $this->_parameters[Import::FIELD_FIELD_MULTIPLE_VALUE_SEPARATOR];
        }
        return Import::DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR;
    }

    /**
     * Get store resolver
     *
     * @return StoreResolver
     */
    public function getStoreResolver(): StoreResolver
    {
        return $this->storeResolver;
    }

    /**
     * Get local images base dir
     *
     * @return ReadInterface
     */
    public function getImageBaseDir(): ReadInterface
    {
        $root = $this->filesystem->getDirectoryRead(DirectoryList::ROOT);
        if (isset($this->_parameters[Import::FIELD_NAME_IMG_FILE_DIR])
            && strlen($this->_parameters[Import::FIELD_NAME_IMG_FILE_DIR])) {
            $path = $root->getAbsolutePath($this->_parameters[Import::FIELD_NAME_IMG_FILE_DIR]);
            return $this->filesystem->getDirectoryReadByPath($path);
        } else {
            $path = $root->getAbsolutePath('var/tmp');
            return $this->filesystem->getDirectoryReadByPath($path);
        }
    }

    /**
     * Get image import base dir
     *
     * @return string|null
     */
    public function getImageImportBaseDir(): ?string
    {
        if (isset($this->_parameters[Import::FIELD_NAME_IMG_FILE_DIR])) {
            return $this->_parameters[Import::FIELD_NAME_IMG_FILE_DIR];
        } else {
            return null;
        }
    }

    /**
     * Get category model type
     *
     * @return CategoryType
     */
    public function getCategoryType(): CategoryType
    {
        return $this->categoryType;
    }

    /**
     * @inheritDoc
     *
     * @return bool|ProcessingErrorAggregatorInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function validateData()
    {
        $valid = parent::validateData();
        if ($valid) {
            $valid = $this->skgValidation();
        }
        return $valid;
    }

    /**
     * Validate SKG dry-run result
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    private function skgValidation(): bool
    {
        $valid = true;
        $createAndDelete = array_intersect_key(
            $this->skgProcessor->getNewSkg(),
            $this->skgProcessor->getDeletedSkg()
        );
        if (count($createAndDelete) > 0) {
            $valid = false;
            foreach ($createAndDelete as $skg => $val) {
                $this->addRowError(
                    RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE,
                    $this->skgProcessor->getNewSkg((string)$skg)['row_num']
                );
                $this->addRowError(
                    RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE,
                    $this->skgProcessor->getDeletedSkg((string)$skg)
                );
            }
        }
        $moveAndDelete = array_intersect_key(
            $this->skgProcessor->getMovedSkg(),
            $this->skgProcessor->getDeletedSkg()
        );
        if (count($moveAndDelete) > 0) {
            $valid = false;
            foreach ($moveAndDelete as $skg => $val) {
                $this->addRowError(
                    RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE,
                    $this->skgProcessor->getMovedSkg((string)$skg)['row_num']
                );
                $this->addRowError(
                    RowValidatorInterface::ERROR_DUPLICATE_UNIQUE_ATTRIBUTE,
                    $this->skgProcessor->getDeletedSkg((string)$skg)
                );
            }
        }
        $resultSkgs = $this->getSkgProcessor()->getResultSkgs();
        $newParents = array_flip(
            array_merge(
                array_map(
                    function ($val) {
                        return $val['parent_skg'];
                    },
                    $this->skgProcessor->getNewSkg()
                ),
                array_map(
                    function ($value) {
                        return $value['new_parent_skg'];
                    },
                    $this->skgProcessor->getMovedSkg()
                )
            )
        );
        $noParent = array_diff_key($newParents, $resultSkgs);
        if (count($noParent) > 0) {
            $valid = false;
            foreach ($noParent as $skg => $val) {
                $wrongParentRow = $this->skgProcessor->getNewSkg((string)$skg)['row_num']
                    ?? $this->skgProcessor->getMovedSkg((string)$skg)['row_num'];
                $this->addRowError(
                    RowValidatorInterface::ERROR_WRONG_PARENT,
                    $wrongParentRow
                );
            }
        }
        return $valid;
    }

    /**
     * Get SKG processor
     *
     * @return SkgProcessor
     */
    public function getSkgProcessor(): SkgProcessor
    {
        return $this->skgProcessor;
    }

    /**
     * Import data rows
     *
     * @return boolean
     * @throws Zend_Validate_Exception
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine Squiz.NamingConventions.ValidVariableName.PrivateNoUnderscore
    protected function _importData()
    {
        set_time_limit(0);
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
            }
        }
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entitiesToCreate = [];
            $entitiesToUpdate = [];
            $entitiesToDelete = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                $rowAction = $this->getRowAction($rowData);
                if ($rowAction == self::ACTION_DELETE) {
                    $entitiesToDelete[$rowNum] = [
                        '__store_id' => $this->getRowStore($rowData),
                        '__skg_to_delete' => $rowData[$this->masterAttributeCode]
                    ];
                } elseif ($rowAction == self::ACTION_UPDATE) {
                    $data = $this->categoryType->prepareAttributesWithDefaultValueForSave($rowData);
                    if ($this->processor->doUpdate(
                        $data,
                        $rowData[self::COL_PARENT_SKG],
                        $this->getRowScope($rowData) == self::SCOPE_DEFAULT
                            ? 0 : (int)$this->_storeCodeToId[$rowData[self::COL_STORE]]
                    )) {
                        $entitiesToUpdate[$rowNum] = [
                            '__store_id' => $this->getRowStore($rowData),
                            '__parent_skg' => $rowData[self::COL_PARENT_SKG],
                            '__data_to_update' => $data
                        ];
                    }
                } else {
                    $entitiesToCreate[$rowNum] = [
                        '__store_id' => $this->getRowStore($rowData),
                        '__parent_skg' => $rowData[self::COL_PARENT_SKG],
                        '__data_to_create' => $this->categoryType->prepareAttributesWithDefaultValueForSave($rowData)
                    ];
                }
            }
            $this->updateItemsCounterStats(
                $entitiesToCreate,
                $entitiesToUpdate,
                $entitiesToDelete
            );
            if ($entitiesToCreate) {
                $this->processor->createCategories($entitiesToCreate);
            }
            if ($entitiesToUpdate) {
                $this->processor->updateCategories($entitiesToUpdate);
            }
            if ($entitiesToDelete) {
                $this->processor->deleteCategories($entitiesToDelete);
            }
        }
        return true;
    }

    /**
     * Validate data row
     *
     * @param array $rowData
     * @param int $rowNumber
     *
     * @return bool
     * @throws Zend_Validate_Exception
     * @throws NoSuchEntityException
     * @throws NoSuchEntityException
     */
    public function validateRow(array $rowData, $rowNumber)
    {
        if (isset($this->_validatedRows[$rowNumber])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNumber);
        }
        $this->_validatedRows[$rowNumber] = true;
        $rowScope = $this->getRowScope($rowData);
        /** @var ?string $skg */
        $skg = null;
        if (!empty($rowData)) {
            $skg = (string)$rowData[self::COL_SKG];
        }
        if (!$this->validator->isValid($rowData)) {
            foreach ($this->validator->getMessages() as $message) {
                $this->skipRow(
                    $rowNumber,
                    $message,
                    ProcessingError::ERROR_LEVEL_NOT_CRITICAL,
                    $this->validator->getInvalidAttribute()
                );
            }
        }
        if ($skg == null) {
            $this->skipRow($rowNumber, RowValidatorInterface::ERROR_SKG_IS_EMPTY);
        } elseif ($skg === false) {
            $this->skipRow($rowNumber, RowValidatorInterface::ERROR_ROW_IS_ORPHAN);
        } elseif ($rowScope == self::SCOPE_STORE
            && !$this->storeResolver->getStoreCodeToId($rowData[self::COL_STORE])
        ) {
            $this->skipRow($rowNumber, RowValidatorInterface::ERROR_INVALID_SCOPE);
        }
        $rowAction = $this->getRowAction($rowData);
        $valid = !$this->getErrorAggregator()->isRowInvalid($rowNumber);
        if ($valid && $rowAction == self::ACTION_ADD) {
            $this->skgProcessor->addNewSkg(
                $rowData[self::COL_SKG],
                [
                    'parent_skg' => $rowData[self::COL_PARENT_SKG],
                    'name' => $rowData['name'],
                    'is_active' => $rowData['is_active'],
                    'url_key' => $rowData['url_key'],
                    'row_num' => $rowNumber
                ]
            );
        } elseif ($rowAction == self::ACTION_DELETE) {
            $this->skgProcessor->deleteSkg($rowData[self::COL_SKG], $rowNumber);
        }
        if ($valid && $rowAction != self::ACTION_DELETE) {
            if ($rowData[self::COL_PARENT_SKG] != $this->skgProcessor->getOldSkg('skg')['parent_skg']) {
                $this->skgProcessor->moveSkg(
                    $skg,
                    [
                        'parent_skg' => $this->skgProcessor->getOldSkg(),
                        'new_parent_skg' => $rowData[self::COL_PARENT_SKG],
                        'row_num' => $rowNumber
                    ]
                );
            }
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNumber);
    }

    /**
     * Get scope for import row
     *
     * @param array $rowData The import data row
     *
     * @return int
     */
    public function getRowScope(array $rowData): int
    {
        if (empty($rowData[self::COL_STORE]) || $rowData[self::COL_STORE] == 'admin') {
            return self::SCOPE_DEFAULT;
        }
        return self::SCOPE_STORE;
    }

    /**
     * Add row as skipped
     *
     * @param int $rowNum
     * @param string $errorCode Error code or simply column name
     * @param string $errorLevel error level
     * @param string|null $colName optional column name
     *
     * @return $this
     */
    private function skipRow(
        $rowNum,
        string $errorCode,
        string $errorLevel = ProcessingError::ERROR_LEVEL_NOT_CRITICAL,
        $colName = null
    ): self {
        $this->addRowError($errorCode, $rowNum, $colName, null, $errorLevel);
        $this->getErrorAggregator()->addRowToSkip($rowNum);
        return $this;
    }

    /**
     * Get action for import row
     *
     * @param array $rowData The import data row
     *
     * @return mixed|string
     */
    public function getRowAction(array $rowData)
    {
        if (isset($rowData[self::COLUMN_ACTION]) &&
            in_array(
                strtolower($rowData[self::COLUMN_ACTION]),
                [self::ACTION_UPDATE, self::ACTION_ADD, self::ACTION_DELETE]
            )) {
            return $rowData[self::COLUMN_ACTION];
        }
        $behavior = strtolower($this->_parameters['behavior'] ?? self::getDefaultBehavior());
        if ($behavior == self::ACTION_DELETE) {
            return self::ACTION_DELETE;
        } elseif ($behavior == Import::BEHAVIOR_REPLACE) {
            return self::ACTION_UPDATE;
        } else {
            $collection = $this->categoryRepository->getBySkg($rowData[self::COL_SKG]);
            if ($collection !== null) {
                return self::ACTION_UPDATE;
            } else {
                return self::ACTION_ADD;
            }
        }
    }

    /**
     * Get store id of row
     *
     * @param array $rowData
     *
     * @return int
     */
    public function getRowStore(array $rowData): int
    {
        if ($this->getRowScope($rowData) == self::SCOPE_DEFAULT) {
            return 0;
        } else {
            return (int)$this->_storeCodeToId[$rowData[self::COL_STORE]];
        }
    }
}
