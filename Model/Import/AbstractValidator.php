<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Abstract import validator
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 21.09.2019
 * Time: 11:15
 */

namespace OooAst\ImportExport\Model\Import;

abstract class AbstractValidator extends \Magento\Framework\Validator\AbstractValidator implements RowValidatorInterface
{
    /**
     * Import context
     *
     * @var AbstractEntity
     */
    protected $context;
    /**
     * Data array to validate
     *
     * @var array
     */
    protected $rowData;
    /**
     * @var array
     */
    private $validators;
    /**
     * Invalid attribute name if it's found
     *
     * @var string|null
     */
    private $invalidAttribute = null;
    /**
     * @var AbstractValidator|null
     */
    private $parentValidator;

    /**
     * AbstractValidator constructor.
     *
     * @param array $validators
     */
    public function __construct(
        array $validators = []
    ) {
        $this->validators = $validators;
    }

    /**
     * Init validators
     *
     * @param AbstractEntity $context
     * @param AbstractValidator|null $parentValidator
     */
    public function init(AbstractEntity $context, ?AbstractValidator $parentValidator)
    {
        $this->parentValidator = $parentValidator;
        $this->context = $context;
        /** @var RowValidatorInterface $validator */
        foreach ($this->validators as $validator) {
            $validator->init($context, $this);
        }
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValid($value): bool
    {
        $this->rowData = $value;
        $this->_clearMessages();
        if (!is_array($value)) {
            $this->_addMessages(
                [
                    $this->context->getMessageTemplate(self::ERROR_ROW_IS_ORPHAN)
                ]
            );
            return false;
        }
        $returnValue = true;
        foreach ($this->validators as $validator) {
            if (!$validator->isValid($value)) {
                $returnValue = false;
                $this->_addMessages($validator->getMessages());
            }
        }
        return $returnValue;
    }

    /**
     * Get invalid attribute name
     *
     * @return string|null
     */
    public function getInvalidAttribute(): ?string
    {
        return $this->invalidAttribute;
    }

    /**
     * Set name of invalid attribute
     *
     * @param string|null $name
     */
    public function setInvalidAttribute(?string $name)
    {
        $this->invalidAttribute = $name;
        if ($this->parentValidator != null) {
            $this->parentValidator->setInvalidAttribute($name);
        }
    }
}
