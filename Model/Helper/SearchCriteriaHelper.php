<?php
/**
 * PHP version 7.1
 * Category import skg search criteria trait
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 23.04.2019
 * Time: 9:36
 */

namespace OooAst\ImportExport\Model\Helper;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use OooAst\ImportExport\Model\Import\Category;

/**
 * Class search criteria helper
 *
 * @package OooAst\ImportExport\Model\Import\Category
 */
class SearchCriteriaHelper
{
    const COL_PARENT_ID = "parent_id";
    /**
     * Search criteria
     *
     * @var SearchCriteriaBuilder
     */
    private $criteriaBuilder;

    /**
     * SkgCriteriaTrait constructor.
     *
     * @param SearchCriteriaBuilder $criteriaBuilder
     */
    public function __construct(
        SearchCriteriaBuilder $criteriaBuilder
    ) {
        $this->criteriaBuilder = $criteriaBuilder;
    }

    /**
     * Create skg search criteria
     *
     * @param string $skg
     *
     * @return SearchCriteria
     */
    public function createSkgCriteria(string $skg): SearchCriteria
    {
        return $this->criteriaBuilder
            ->addFilter(Category::COL_SKG, $skg, 'eq')
            ->create();
    }

    /**
     * Create criteria to search categories by parent id
     *
     * @param int $parentId
     *
     * @return SearchCriteria
     */
    public function createParentCriteria(int $parentId): SearchCriteria
    {
        return $this->criteriaBuilder
            ->addFilter(self::COL_PARENT_ID, $parentId, 'eq')
            ->create();
    }

    /**
     * Create empty search criteria
     *
     * @return SearchCriteria
     */
    public function createEmptyCriteria(): SearchCriteria
    {
        return $this->criteriaBuilder->create();
    }
}
