<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Image name utils
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 13.08.2019
 * Time: 21:39
 */

namespace OooAst\ImportExport\Model\Helper;

use Magento\Framework\Url\Validator;

class ImageName
{
    /**
     * @var Validator
     */
    private $urlValidator;

    /**
     * ImageName constructor.
     *
     * @param Validator $urlValidator
     */
    public function __construct(
        Validator $urlValidator
    ) {
        $this->urlValidator = $urlValidator;
    }

    /**
     * Create mage like image file name
     *
     * @param string $newName
     *
     * @return string
     */
    public function imageIndexedName(string $newName): string
    {
        if ($this->isUrl($newName)) {
            $url = curl_init($newName);
            $basename = basename(curl_getinfo($url, CURLINFO_EFFECTIVE_URL));
        } else {
            $basename = basename($newName);
        }
        return '/'. substr($basename, 0, 1)
            . '/' . substr($basename, 1, 1)
            . '/' . $basename;
    }

    /**
     * Check if parameter is valid url
     *
     * @param string $url
     *
     * @return bool
     */
    public function isUrl(string $url): bool
    {
        return $this->urlValidator->isValid($url);
    }
}
