<?php
/**
 * PHP version 7.1
 * Category import repository
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 24.04.2019
 * Time: 10:46
 */
declare(strict_types=1);

namespace OooAst\ImportExport\Model\Helper;

use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryManagementInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\Data\CategoryInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Category repository for import/export purpose
 *
 * @package OooAst\ImportExport\Model\Import\Category
 */
class CategoryRepository
{
    /**
     * Magento category repository implementation
     *
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * Magento category list implementation
     *
     * @var CategoryListInterface
     */
    private $categoryList;
    /**
     * Category factory
     *
     * @var CategoryInterfaceFactory
     */
    private $categoryFactory;
    /**
     * Search criteria helper
     *
     * @var SearchCriteriaHelper
     */
    private $criteriaHelper;
    /**
     * Category manager
     *
     * @var CategoryManagementInterface
     */
    private $categoryManagement;

    /**
     * CategoryRepository constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryListInterface $categoryList
     * @param CategoryInterfaceFactory $categoryFactory
     * @param CategoryManagementInterface $categoryManagement
     * @param SearchCriteriaHelper $criteriaHelper
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        CategoryListInterface $categoryList,
        CategoryInterfaceFactory $categoryFactory,
        CategoryManagementInterface $categoryManagement,
        SearchCriteriaHelper $criteriaHelper
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->categoryList = $categoryList;
        $this->categoryFactory = $categoryFactory;
        $this->categoryManagement = $categoryManagement;
        $this->criteriaHelper = $criteriaHelper;
    }

    /**
     * Get category list
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return CategoryInterface[]
     */
    public function getList(SearchCriteriaInterface $searchCriteria): array
    {
        return $this->categoryList->getList($searchCriteria)->getItems();
    }

    /**
     * Save category
     *
     * @param CategoryInterface $category
     *
     * @return CategoryInterface|null Null - can not save, value - saved category
     */
    public function save(CategoryInterface $category): ?CategoryInterface
    {
        try {
            return $this->categoryRepository->save($category);
        } catch (CouldNotSaveException $e) {
            return null;
        }
    }

    /**
     * Get info about category by category id
     *
     * @param int $categoryId
     * @param int $storeId
     *
     * @return CategoryInterface|null NULL - not found, value - category
     */
    public function get(int $categoryId, ?int $storeId = null): ?CategoryInterface
    {
        try {
            return $this->categoryRepository->get($categoryId, $storeId);
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Get category by search criteria
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param int|null $storeId
     *
     * @return CategoryInterface|null The found category or null if category does not exist
     */
    public function getBySearchCriteria(
        SearchCriteriaInterface $searchCriteria,
        ?int $storeId = null
    ): ?CategoryInterface {
        $cat = current(
            $this->categoryList->getList($searchCriteria)->getItems()
        );
        if (!$cat) {
            return null;
        } else {
            try {
                return $this->categoryRepository->get($cat->getId(), $storeId);
            } catch (NoSuchEntityException $e) {
                return null;
            }
        }
    }

    /**
     * Get category by skg
     *
     * @param string $skg
     * @param int|null $storeId
     *
     * @return CategoryInterface|null The found category or false otherwise
     */
    public function getBySkg(string $skg, ?int $storeId = null): ?CategoryInterface
    {
        $cat = current(
            $this->categoryList->getList($this->criteriaHelper->createSkgCriteria($skg))
                ->getItems()
        );
        if (!$cat) {
            return null;
        } else {
            try {
                return $this->categoryRepository->get($cat->getId(), $storeId);
            } catch (NoSuchEntityException $e) {
                return null;
            }
        }
    }

    /**
     * Create new category entity
     *
     * @return CategoryInterface
     */
    public function create(): CategoryInterface
    {
        return $this->categoryFactory->create();
    }

    /**
     * Delete category
     *
     * @param CategoryInterface $category
     *
     * @return bool True - deleted, false - error happened
     */
    public function delete(CategoryInterface $category): bool
    {
        try {
            return $this->categoryRepository->delete($category);
        } catch (InputException|NoSuchEntityException|StateException $e) {
            return false;
        }
    }

    /**
     * Delete category by id
     *
     * @param int $categoryId
     *
     * @return bool True - deleted, false - error happened
     */
    public function deleteByIdentifier(int $categoryId): bool
    {
        try {
            return $this->categoryRepository->deleteByIdentifier($categoryId);
        } catch (InputException |NoSuchEntityException |StateException $e) {
            return false;
        }
    }

    /**
     * Move category to new parent
     *
     * @param int $categoryId
     * @param int $parentId
     * @param int|null $afterId
     *
     * @return bool True - moved, false - error happened
     */
    public function move(int $categoryId, int $parentId, int $afterId = null): bool
    {
        try {
            return $this->categoryManagement->move($categoryId, $parentId, $afterId);
        } catch (NoSuchEntityException |LocalizedException $e) {
            return false;
        }
    }
}
