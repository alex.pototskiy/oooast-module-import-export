<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 12:16
 */

namespace OooAst\ImportExport\Model;

use Magento\Framework\Data\Collection as AttributeCollection;
use Magento\Framework\Exception\LocalizedException;
use OooAst\ImportExport\Api\ColumnProviderInterface;

/**
 * Class ColumnProvider
 *
 * @package OooAst\ImportExport\Model\Export
 */
class ColumnProvider implements ColumnProviderInterface
{
    /**
     * @var array The column to header map
     */
    private $columnHeaderMap;

    /**
     * ColumnProvider constructor.
     *
     * @param array $columnHeaderMap
     */
    public function __construct(
        array $columnHeaderMap
    ) {
        $this->columnHeaderMap = $columnHeaderMap;
    }

    /**
     * Get filtered columns
     *
     * @param AttributeCollection $attributes The attribute collection
     * @param array $filters The filter array
     *
     * @return array
     * @throws LocalizedException
     */
    public function getColumns(AttributeCollection $attributes, array $filters): array
    {
        return $this->filterAttributes($attributes, $filters);
    }

    /**
     * Filter attributes
     *
     * @param AttributeCollection $attributes
     * @param array $filters
     *
     * @return array
     * @throws LocalizedException
     */
    private function filterAttributes(AttributeCollection $attributes, array $filters): array
    {
        $attrNames = [];
        foreach ($attributes->getItems() as $item) {
            $attrNames[] = $item->getData('id');
        }

        if (count($filters) == 0) {
            return $attrNames;
        }

        if (count($filters['skip_attr']) === count($attrNames)) {
            throw new LocalizedException(__('There is no data for the export.'));
        }

        return array_diff($attrNames, $filters);
    }

    /**
     * Get filtered headers
     *
     * @param AttributeCollection $attributes The attribute collection
     * @param array $filters The filter array
     *
     * @return array
     * @throws LocalizedException
     */
    public function getHeaders(AttributeCollection $attributes, array $filters): array
    {
        return array_map(
            function ($value) {
                if (isset($this->columnHeaderMap[$value])) {
                    return $this->columnHeaderMap[$value];
                } else {
                    return $value;
                }
            },
            $this->filterAttributes($attributes, $filters)
        );
    }
}
