<?php
declare(strict_types=1);
/**
 * PHP version 7.1
 * Old product image cleaning after import
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 26.07.2019
 * Time: 14:07
 */

namespace OooAst\ImportExport\Observer;

use Magento\Catalog\Model\Product\Gallery\GalleryManagement;
use Magento\CatalogImportExport\Model\Import\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\Store\Model\Store;
use OooAst\ImportExport\Model\Import\Product\MediaProcessor;
use OooAst\ImportExport\Model\Import\Product\ProductMedia;

/**
 * Class ProcessProductImageImport
 *
 * @package OooAst\ImportExport\Observer
 */
class ProcessProductImageImport implements ObserverInterface
{
    const COL_PRODUCT_MEDIA = 'product_media';
    /**
     * @var ProductMedia
     */
    private $productMedia;
    /**
     * @var Product
     */
    private $adapter;
    /**
     * @var GalleryManagement
     */
    private $galleryManagement;
    /**
     * @var MediaProcessor
     */
    private $mediaProcessor;

    /**
     * ProcessProductImageImport constructor.
     *
     * @param ProductMedia $productMedia
     * @param GalleryManagement $galleryManagement
     * @param MediaProcessor $mediaProcessor
     */
    public function __construct(
        ProductMedia $productMedia,
        GalleryManagement $galleryManagement,
        MediaProcessor $mediaProcessor
    ) {
        $this->productMedia = $productMedia;
        $this->galleryManagement = $galleryManagement;
        $this->mediaProcessor = $mediaProcessor;
    }

    /**
     * Update product images
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $mediaGallery = [];
        $labelsForUpdate = [];
        $imagesForChangeVisibility = [];
        $uploadedImages = [];
        $existingImagesFullInfo = [];
        $collectedRowImages = [];
        $attributes = [];
        $previousType = null;
        $prevAttributeSet = null;

        $this->adapter = $observer->getData('adapter');
        $this->productMedia->setParameters($this->adapter->getParameters());
        /** @var array $bunch */
        $bunch = $observer->getData('bunch');
        foreach ($bunch as $rowNum => $rowData) {
            $rowScope = $this->adapter->getRowScope($rowData);
            $rowSku = $rowData[Product::COL_SKU];
            $storeId = !empty($rowData[Product::COL_STORE])
                ? $this->adapter->getStoreIdByCode($rowData[Product::COL_STORE])
                : Store::DEFAULT_STORE_ID;

            $mediaData = $rowData[self::COL_PRODUCT_MEDIA];
            if ($mediaData === null || empty($mediaData)) {
                $collectedRowImages[$storeId][$rowSku] = [];
                continue;
            }
            $existingImagesFullInfo = $this->mediaProcessor->getExistingImages(
                array_column($bunch, Product::COL_SKU)
            );
            $existingImages = [];
            foreach ($existingImagesFullInfo as $skuImage) {
                foreach ($skuImage as $sku => $image) {
                    $existingImages[$sku] = $image;
                }
            }

            $mediaData = explode($this->adapter->getMultipleValueSeparator(), $mediaData);
            $vRowData = [];
            foreach ($mediaData as $media) {
                list($key, $value) = explode(Product::PAIR_NAME_VALUE_SEPARATOR, $media);
                $vRowData[$this->productMedia->getMediaKey($key)] = str_replace(
                    Product::PSEUDO_MULTI_LINE_SEPARATOR,
                    $this->adapter->getMultipleValueSeparator(),
                    $value
                );
            }
            list($rowImages, $rowLabels, $imageHiddenStates, $uploadedImages) = $this->getImageFromRow(
                $vRowData,
                $rowSku,
                $existingImages,
                $uploadedImages
            );

            if (!empty($rowImages['_media_image'])) {
                foreach ($rowImages['_media_image'] as $image) {
                    $collectedRowImages[$storeId][$rowSku][$image] = 1;
                }
            } else {
                $collectedRowImages[$storeId][$rowSku] = [];
            }
            $vRowData[Product::COL_MEDIA_IMAGE] = [];

            $this->getMediaGallery(
                $rowImages,
                $uploadedImages,
                $vRowData,
                $rowNum,
                $mediaGallery,
                $storeId,
                $rowSku,
                $existingImages,
                $rowLabels,
                $labelsForUpdate,
                $imageHiddenStates,
                $imagesForChangeVisibility
            );

            // attributes
            $rowStore = (Product::SCOPE_STORE == $rowScope)
                ? $this->productMedia->getStoreResolver()->getStoreCodeToId($rowData[self::COL_STORE])
                : 0;
            $productType = isset($rowData[Product::COL_TYPE]) ? $rowData[Product::COL_TYPE] : null;
            if ($productType !== null) {
                $previousType = $productType;
            }
            if (isset($rowData[Product::COL_ATTR_SET])) {
                $prevAttributeSet = $rowData[Product::COL_ATTR_SET];
            }
            if (Product::SCOPE_NULL == $rowScope) {
                // for multiselect attributes only
                if ($prevAttributeSet !== null) {
                    $rowData[Product::COL_ATTR_SET] = $prevAttributeSet;
                }
                if ($productType === null && $previousType !== null) {
                    $productType = $previousType;
                }
                if ($productType === null) {
                    continue;
                }
            }

            $product = $this->productMedia->getProxyProdFactory()->create(['data' => $vRowData]);

            foreach ($vRowData as $attrCode => $attrValue) {
                $attribute = $this->adapter->retrieveAttributeByCode($attrCode);
                if (!$attribute) {
                    continue;
                }

                if ('multiselect' != $attribute->getFrontendInput() && Product::SCOPE_NULL == $rowScope) {
                    // skip attribute processing for SCOPE_NULL rows
                    continue;
                }
                $attrId = $attribute->getId();
                $backModel = $attribute->getBackendModel();
                $attrTable = $attribute->getBackend()->getTable();
                $storeIds = [0];

                if ($backModel) {
                    $attribute->getBackend()->beforeSave($product);
                    $attrValue = $product->getData($attribute->getAttributeCode());
                }

                if (Product::SCOPE_STORE == $rowScope) {
                    if (Product::SCOPE_WEBSITE == $attribute->getIsGlobal()) {
                        // check website defaults already set
                        if (!isset($attributes[$attrTable][$rowSku][$attrId][$rowStore])) {
                            $storeIds = $this->productMedia->getStoreResolver()->getStoreIdToWebsiteStoreIds($rowStore);
                        }
                    } elseif (Product::SCOPE_STORE == $attribute->getIsGlobal()) {
                        $storeIds = [$rowStore];
                    }
                    if (!$this->productMedia->isSkuExist($rowSku)) {
                        $storeIds[] = 0;
                    }
                }
                foreach ($storeIds as $storeId) {
                    if (!isset($attributes[$attrTable][$rowSku][$attrId][$storeId])) {
                        $attributes[$attrTable][$rowSku][$attrId][$storeId] = $attrValue;
                    }
                }
                // restore 'backend_model' to avoid 'default' setting
                $attribute->setBackendModel($backModel);
            }

        }
        $this->mediaProcessor->removeProductImages($existingImagesFullInfo, $collectedRowImages);
        $this->productMedia->saveMediaGalleryWrapper($mediaGallery);
        $this->productMedia->updateMediaGalleryVisibility($imagesForChangeVisibility);
        $this->productMedia->updateMediaGalleryLabels($labelsForUpdate);
        $this->productMedia->saveProductAttributesWrapper($attributes);
    }

    /**
     * @param array $vRowData
     * @param $rowSku
     * @param array $existingImages
     * @param array $uploadedImages
     *
     * @return array
     */
    private function getImageFromRow(array $vRowData, $rowSku, array $existingImages, array $uploadedImages): array
    {
        list($rowImages, $rowLabels) = $this->adapter->getImagesFromRow($vRowData);
        $imageHiddenStates = $this->productMedia->getImagesHiddenStates($vRowData);
        foreach (array_keys($imageHiddenStates) as $image) {
            if (array_key_exists($rowSku, $existingImages)
                && array_key_exists($image, $existingImages[$rowSku])
            ) {
                $rowImages[Product::COL_MEDIA_IMAGE][] = $image;
                $uploadedImages[$image] = $image;
            }

            if (empty($rowImages)) {
                $rowImages[Product::COL_MEDIA_IMAGE][] = $image;
            }
        }
        return array($rowImages, $rowLabels, $imageHiddenStates, $uploadedImages);
    }

    /**
     * @param $rowImages
     * @param $uploadedImages
     * @param array $vRowData
     * @param $rowNum
     * @param array $mediaGallery
     * @param $storeId
     * @param $rowSku
     * @param array $existingImages
     * @param $rowLabels
     * @param array $labelsForUpdate
     * @param $imageHiddenStates
     * @param array $imagesForChangeVisibility
     */
    private function getMediaGallery(
        $rowImages,
        &$uploadedImages,
        array &$vRowData,
        $rowNum,
        array &$mediaGallery,
        $storeId,
        $rowSku,
        array &$existingImages,
        $rowLabels,
        array &$labelsForUpdate,
        $imageHiddenStates,
        array &$imagesForChangeVisibility
    ): void {
        $position = 0;
        foreach ($rowImages as $column => $columnImages) {
            foreach ($columnImages as $columnImageKey => $columnImage) {
                if (!isset($uploadedImages[$columnImage])) {
                    $uploadedFile = $this->productMedia->uploadMediaFilesWrapper($columnImage);
                    $uploadedFile = $uploadedFile ?: $this->productMedia->getSystemFile($columnImage);
                    if ($uploadedFile) {
                        $uploadedImages[$columnImage] = $uploadedFile;
                    } else {
                        unset($vRowData[$column]);
                        $this->adapter->addRowError(
                            Product\RowValidatorInterface::ERROR_MEDIA_URL_NOT_ACCESSIBLE,
                            $rowNum,
                            null,
                            null,
                            ProcessingError::ERROR_LEVEL_NOT_CRITICAL
                        );
                    }
                } else {
                    $uploadedFile = $uploadedImages[$columnImage];
                }

                if ($uploadedFile && $column !== Product::COL_MEDIA_IMAGE) {
                    $vRowData[$column] = $uploadedFile;
                }

                if ($uploadedFile && !isset($mediaGallery[$storeId][$rowSku][$uploadedFile])) {
                    if (isset($existingImages[$rowSku][$uploadedFile])) {
                        $currentFileData = $existingImages[$rowSku][$uploadedFile];
                        if (isset($rowLabels[$column][$columnImageKey])
                            && $rowLabels[$column][$columnImageKey] !=
                            $currentFileData['label']
                        ) {
                            $labelsForUpdate[] = [
                                'label' => $rowLabels[$column][$columnImageKey],
                                'imageData' => $currentFileData
                            ];
                        }

                        if (array_key_exists($uploadedFile, $imageHiddenStates)
                            && $currentFileData['disabled'] != $imageHiddenStates[$uploadedFile]
                        ) {
                            $imagesForChangeVisibility[] = [
                                'disabled' => $imageHiddenStates[$uploadedFile],
                                'imageData' => $currentFileData
                            ];
                        }
                    } else {
                        if ($column == Product::COL_MEDIA_IMAGE) {
                            $vRowData[$column][] = $uploadedFile;
                        }
                        $mediaGallery[$storeId][$rowSku][$uploadedFile] = [
                            'attribute_id' => $this->adapter->getMediaGalleryAttributeId(),
                            'label' => isset($rowLabels[$column][$columnImageKey])
                                ? $rowLabels[$column][$columnImageKey]
                                : '',
                            'position' => ++$position,
                            'disabled' => isset($imageHiddenStates[$columnImage])
                                ? $imageHiddenStates[$columnImage] : '0',
                            'value' => $uploadedFile,
                        ];
                    }
                }
            }
        }
    }

    /**
     * Get field separator
     *
     * @return String
     */
    protected function getFieldSeparator(): String
    {
        if (!empty($this->adapter->getParameters()[Import::FIELD_FIELD_SEPARATOR])) {
            return $this->adapter->getParameters()[Import::FIELD_FIELD_SEPARATOR];
        }
        return ',';
    }

}
