<?php
/**
 * PHP version 7.1
 *
 * Column provider for import/export models
 *
 * @category ImportExport
 * @package  OooAst
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 09.04.2019
 * Time: 9:14
 */

namespace OooAst\ImportExport\Api;

use Magento\Framework\Data\Collection as AttributeCollection;

/**
 * Interface ColumnProviderInterface
 *
 * @package OooAst\Core\Api\ImportExport
 * @api
 */
interface ColumnProviderInterface
{
    /**
     * Returns header names for exported file
     *
     * @param AttributeCollection $attributeCollection
     * @param array $filters
     * @return array
     */
    public function getHeaders(AttributeCollection $attributeCollection, array $filters): array;

    /**
     * Returns column names for Collection Select
     *
     * @param AttributeCollection $attributeCollection
     * @param array $filters
     * @return array
     */
    public function getColumns(AttributeCollection $attributeCollection, array $filters): array;

}
