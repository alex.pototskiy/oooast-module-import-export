<?php
/**
 * PHP version 7.1
 * ${File_description}
 *
 * @category ${Category}
 * @package  ${PACKAGE}
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     ${Link_description}
 * Date: 12.12.2018
 * Time: 12:45
 */

namespace OooAst\ImportExport\Api;

use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\Collection as AttributeCollection;

interface CollectionBuilderInterface
{
    /**
     * SourceItemCollection is used to gather all the data (with filters applied) which need to be exported
     *
     * @param AttributeCollection $attributes The attribute collection
     * @param array $skipAttrs Attributes to skip
     * @param array $filters The filter array
     *
     * @return Collection
     */
    public function create(AttributeCollection $attributes, array $skipAttrs, array $filters): Collection;
}
