<?php
/**
 * PHP version 7.1
 *
 * Category import-export tools
 *
 * @category ImportExport
 * @package  OooAst_ImportExportCatgory
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 * Date: 16.04.2019
 * Time: 8:03
 */

namespace OooAst\ImportExport\Tools;

/**
 * Array comparision tools
 *
 * @category ImportExport
 * @package  OooAst_ImportExportCatgory
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link     http://oooast-site/admin
 */
class Tools
{
    /**
     * Get array difference recursively (based on keys)
     *
     * @param array $array1 The first array
     * @param array $array2 The second array
     *
     * @return array
     */
    public static function arrayDiffAssocRecursive($array1, $array2)
    {
        $difference = [];
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) || !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $newDiff = Tools::arrayDiffAssocRecursive($value, $array2[$key]);
                    if (!empty($newDiff)) {
                        $difference[$key] = $newDiff;
                    }
                }
            } else {
                if (!array_key_exists($key, $array2) || $array2[$key] !== $value) {
                    $difference[$key] = $value;
                }
            }
        }
        return $difference;
    }
}
