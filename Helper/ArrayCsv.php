<?php
/**
 * PHP version 7.1
 * Array to/from CSV
 *
 * @category ImportExport
 * @package  OooAst_ImportExport
 * @author   Alexander Pototskiy <alex@pototskiy.net>
 * @license  http://opensource.org/licenses/gpl-license.php GPL
 * @link
 * Date: 13.08.2019
 * Time: 15:38
 */

namespace OooAst\ImportExport\Helper;

/**
 * Class ArrayCsv
 *
 * @package OooAst\ImportExport\Helper
 */
class ArrayCsv
{
    /**
     * Serialize array to csv
     *
     * @param array $data
     *
     * @return string
     */
    public static function toCsvString(array $data): string // phpcs:ignore
    {
        $csv = '';
        $stream = fopen('php://memory', 'w+'); // phpcs:ignore
        try {
            fputcsv($stream, $data, ',', '"');
            rewind($stream);
            $csv = trim(stream_get_contents($stream)); // phpcs:ignore
        } finally {
            fclose($stream); // phpcs:ignore
        }
        return $csv;
    }

    /**
     * Deserialize array from csv string
     *
     * @param string $csv
     *
     * @return array
     */
    public static function toArray(string $csv): array // phpcs:ignore
    {
        $array = [];
        $stream = fopen('data://text/plain,' . $csv, 'r'); // phpcs:ignore
        try {
            $array = fgetcsv($stream, 0, ',', '"'); // phpcs:ignore
        } finally {
            fclose($stream); // phpcs:ignore
        }
        return $array;
    }
}
